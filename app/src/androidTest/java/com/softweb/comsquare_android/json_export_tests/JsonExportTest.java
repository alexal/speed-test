package com.softweb.comsquare_android.json_export_tests;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.softweb.comsquare_android.domain.ExportJSONHelper;
import com.softweb.comsquare_android.domain.phone_stats.DeviceInfo;
import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;
import com.softweb.comsquare_android.domain.phone_stats.NetworkInfo;
import com.softweb.comsquare_android.domain.phone_stats.RadioInfo;
import com.softweb.comsquare_android.domain.providers.DeviceInfoProvider;
import com.softweb.comsquare_android.domain.providers.RadioInfoProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationParser;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.PlayServicesLocationProvider;
import com.softweb.comsquare_android.domain.providers.network.IpApiProvider;
import com.softweb.comsquare_android.domain.providers.network.IpProvider;
import com.softweb.comsquare_android.domain.providers.network.NetworkInfoProvider;
import com.softweb.comsquare_android.utils.AppExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import timber.log.Timber;

@RunWith(AndroidJUnit4.class)
public class JsonExportTest {

  private ExportJSONHelper exportJSONHelper; // TODO NOT WORKING

  private NetworkInfo networkInfo;
  private RadioInfo radioInfo;
  private DeviceInfo deviceInfo;
  private LocationInfo locationInfo;

  private NetworkInfoProvider setupNetworkInfoProvider(Context context){

    NetworkInfoProvider.Builder builder = new NetworkInfoProvider.Builder();

    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    final IpProvider ipApiProvider = new IpApiProvider();
    final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);

    builder
        .setpTelephonyManager(telephonyManager)
        .setIpProvider(ipApiProvider)
        .setSubscriptionManager(subscriptionManager)
        .setWifiManager(wifiManager);

    return builder.createNetworkInfoProvider();
  }

  private DeviceInfoProvider setupDeviceInfoProvider(Context context){

    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    final PackageManager packageManager = context.getPackageManager();

    return new DeviceInfoProvider(telephonyManager,packageManager,context);
  }

  private RadioInfoProvider setupRadioInfoProvider(Context context){

    // Setup radio info provider

    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

    return new RadioInfoProvider(telephonyManager,wifiManager);
  }

  private PlayServicesLocationProvider setupLocationProvider(Context context){

    final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
    final LocationParser locationParser = new LocationParser(new Geocoder(context));

    return new PlayServicesLocationProvider(fusedLocationProviderClient,locationParser);
  }

  @Before
  public void setup(){

    final Context context = InstrumentationRegistry.getTargetContext();

    networkInfo = setupNetworkInfoProvider(context).fetchNetworkInfo();
    radioInfo = setupRadioInfoProvider(context).fetchRadioInfo();
    deviceInfo = setupDeviceInfoProvider(context).fetchDeviceInfo();
    setupLocationProvider(context).fetchLocation(new com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationProvider.LocationFetchListener() {
      @Override
      public void onLocationFetched(@NonNull LocationInfo location) {

        locationInfo = location;
      }

      @Override
      public void onError(@NonNull Exception e) {

      }
    });
  }

  @Test
  public void testDeviceInfoExport(){

    String deviceInfoJson = exportJSONHelper.exportDeviceInfo();

    Timber.e("Device info object: %s",deviceInfo.toString());
    Timber.e("Device info JSON: %s \n",deviceInfoJson);

  }

  @Test
  public void testRadioInfoExport(){

    String radioInfoJson = exportJSONHelper.exportRadioInfo();

    Timber.e("Radio info object: %s",radioInfo.toString());
    Timber.e("Radio info JSON: %s \n",radioInfoJson);

  }

  @Test
  public void testLocationInfoExport(){

    String locationInfo = exportJSONHelper.exportLocationInfo();

    Timber.e("Location info object: %s",this.locationInfo.toString());
    Timber.e("Location info JSON: %s \n",locationInfo);

  }

  @Test
  public void testNetworkInfoExport(){

    String networkInfoJson = exportJSONHelper.exportNetworkInfo();

    Timber.e("Network info object: %s",networkInfo.toString());
    Timber.e("Network info JSON: %s \n",networkInfoJson);

  }

  @Test
  public void testAllInfoExport(){

    String allInfoJson = exportJSONHelper.exportAllInfo();

    Timber.e("All info JSON: %s \n",allInfoJson);

  }
}
