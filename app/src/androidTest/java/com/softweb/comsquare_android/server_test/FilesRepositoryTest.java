package com.softweb.comsquare_android.server_test;

import com.softweb.comsquare_android.domain.remote.TestFilesRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;

import timber.log.Timber;

@RunWith(JUnit4.class)
public class FilesRepositoryTest {

  private TestFilesRepository filesRepository;

  @Before
  public void setup(){
    filesRepository = new TestFilesRepository();
  }

  private void showDownloadCompleted(){
    Timber.e("Download completed");
  }

  private void renderProgress(long progress){
    Timber.e("Downloading... %d%% \n", progress);
  }


}
