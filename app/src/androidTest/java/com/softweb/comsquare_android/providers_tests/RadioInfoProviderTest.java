package com.softweb.comsquare_android.providers_tests;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.softweb.comsquare_android.domain.phone_stats.RadioInfo;
import com.softweb.comsquare_android.domain.providers.RadioInfoProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import timber.log.Timber;

import static junit.framework.Assert.*;

/**
 * Class containing tests for {@link RadioInfoProvider}
 * The tests mostly check that the fetched values are actually in their corresponding range.
 *</p>
 * HOW TO RUN CUSTOM TESTS:
 * Because this class depends 100% on the hardware of the test device, you should firstly
 * know the network type of the device (3G,LTE,etc) and adjust each expected result.
 */
@RunWith(AndroidJUnit4.class)
public class RadioInfoProviderTest {


  // ------------------------------------------------------------------->

  private static final int LAC_UKNOWN = 65535;
  private static final int LAC_LTE = -1; // LTE signals does not contain lac value
  private static final int TAC_NON_LTE = -1; // Non-lte signals do not contain tac value
  private static final int PSC_UKNOWN = 65535; //
  private static final int ASU_UKNOWN = 99;
  private static final int RSCP_UKNOWN = -200;
  private static final int ECIO_UKNOWN = 100;
  private static final int SINR_INVALID = 100;
  private static final String SIGNAL_UKNOWN = "signal_uknown";
  private static final int RSRP_UKNOWN = 0;
  private static final int RX_LEVEL_UKNOWN = 100;

  // ------------------------------------------------------------------->

  private RadioInfoProvider radioInfoProvider;
  private RadioInfo radioInfo;

  @Before
  public void setup(){

    final Context context = InstrumentationRegistry.getTargetContext();

    // Setup radio info provider
    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

    radioInfoProvider = new RadioInfoProvider(telephonyManager,wifiManager);
    radioInfo = radioInfoProvider.fetchRadioInfo();
  }

  // ---------------- Tests on Xiaomi Redmi 5 Plus (LTE) --------------------------

  @Test
  public void test_rssi_validation(){
    final int rssi = radioInfo.getRssi();
    Log.d("Validation_Rssi", String.valueOf(rssi));
    assertTrue(rssi <=-25 && rssi >=-100);
  }

  @Test
  public void test_lac_validation(){
    final int lac = radioInfo.getLac();
    assertTrue(lac == LAC_UKNOWN );
  }

  @Test
  public void test_tac_validation(){
    assertTrue(radioInfo.getTac() != 65535);
  }

  @Test
  public void test_psc_validation(){
    assertTrue(radioInfo.getPsc() == PSC_UKNOWN);
  }

  @Test
  public void test_asu_level_validation(){
    final int asu = radioInfo.getAsu();
    assertTrue(asu > 0 && asu < 97);
  }

  @Test
  public void test_rscp_level_validation(){
    final int rscp = radioInfo.getRscp();
    assertTrue(rscp >= -120 && rscp <= -25);
  }

  @Test
  public void test_level_validation(){
    assertTrue(radioInfo.getLevel() <=4 && radioInfo.getLevel() >= 0);
  }

  @Test
  public void test_timing_advance_validation(){

    // This test fails most of the time because the reported LTE value is incorrect propably.
    // And do what? sit and have a fight? nahh...

    final double timingAdvance = radioInfo.getTimingAdvance();
    Timber.d("Reported timing advance = %s",timingAdvance);

    assertTrue(timingAdvance > 0 && timingAdvance <= 1282);
  }

  @Test
  public void test_ecio_validation(){
    Timber.d("Ecio value = %s", radioInfo.getEcio());
    // The value range is ONLY for 3G networks
//    assertTrue(radioInfo.getEcio() <= 0 && radioInfo.getEcio() >= -24);
    assertTrue(radioInfo.getEcio() == ECIO_UKNOWN);
  }

  @Test
  public void test_sinr_validation(){
    Timber.d("SINR = %s",radioInfo.getSinr());
    assertTrue(radioInfo.getSinr() >= -30 && radioInfo.getSinr() <= 30);
  }

  @Test
  public void test_wifi_frequency_band_validation(){
    assertTrue(radioInfo.getWifi_frequency_band_mhz().equals("2.4 GHz") || radioInfo.getWifi_frequency_band_mhz().equals("5 GHz") );
  }

  @Test
  public void test_rsrp_validation(){

    // This value check is valid only for LTE network

    final int rsrp = radioInfo.getRsrp();

    Timber.d("RSRP Value = %s", rsrp);
    assertTrue(rsrp >= -140 && rsrp <= -44);
  }

}
