package com.softweb.comsquare_android.providers_tests;

import android.content.Context;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationParser;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.PlayServicesLocationProvider;
import com.softweb.comsquare_android.utils.AppExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Class containing tests for {@link PlayServicesLocationProvider}.
 * The tests are basically testing the result values by comparing them to YOUR provided ones.
 * Because location provider has to do with location (duh..) the tests are in this specific form.
 *
 * HOW TO RUN CUSTOM TESTS:
 * 1) Setup the expected results in the fields after the class declaration. It is considered
 *    you know all of them. These values concern the test device location.
 * 2) Debug the application with this class running and just check that all have passed.
 *
 */
@RunWith(AndroidJUnit4.class)
public class LocationInfoProviderTest {

    // --------------------- Setup expected location results here ---------------------------------

    private static final String my_country_code = "GR";
    private static final String my_region = "Thessaloniki";
    private static final String my_city = "Thermi";
    private static final String my_country = "Greece";
    private static final String my_timezone = "Europe/Athens";
    private static final String my_timezone_offset = "GMT+03:00";
    private static final double my_speed = 0;
    private static final double my_altitude = 0;

    //---------------------------------------------------------------------------------------------

    private PlayServicesLocationProvider locationProvider;

    @Before
    public void setup(){

        // Init dependencies

        final Context context = InstrumentationRegistry.getTargetContext();
        final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        final LocationParser locationParser = new LocationParser(new Geocoder(context), AppExecutors.getInstance());

        locationProvider = new PlayServicesLocationProvider(fusedLocationProviderClient,locationParser);
    }

    @Test
    public void testCountryCode(){

        locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
            @Override
            public void onLocationFetched(@NonNull LocationInfo locationInfo) {

                assertEquals(my_country_code,
                        locationInfo.getCountryCode());
            }

            @Override
            public void onError(@NonNull Exception e) {

            }
        });
    }

    @Test
    public void testLocationInfoIsNeverNull(){
        locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
            @Override
            public void onLocationFetched(@NonNull LocationInfo location) {
                assertNotNull(location);
            }

            @Override
            public void onError(@NonNull Exception e) {

            }
        });
    }

    @Test
    public void testCountryName(){
      locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
          @Override
          public void onLocationFetched(@NonNull LocationInfo location) {
              assertEquals(my_country,
                      location.getCountry());
          }

          @Override
          public void onError(@NonNull Exception e) {

          }
      });
    }

    @Test
    public void testAltitude(){
        locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
            @Override
            public void onLocationFetched(@NonNull LocationInfo location) {
                assertEquals((int)my_altitude,(int)location.getAltitude());
            }

            @Override
            public void onError(@NonNull Exception e) {

            }
        });
    }

    public void testCity(){
      locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
        @Override
        public void onLocationFetched(@NonNull LocationInfo location) {
          assertEquals(my_city.toLowerCase(),location.getCity().toLowerCase());
        }

        @Override
        public void onError(@NonNull Exception e) {

        }
      });
    }

    @Test
    public void testRegion(){
      locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
        @Override
        public void onLocationFetched(@NonNull LocationInfo location) {
          assertEquals(my_region.toLowerCase(),location.getRegion().toLowerCase());
        }

        @Override
        public void onError(@NonNull Exception e) {

        }
      });
    }

    @Test
    public void testSpeed(){
        locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
            @Override
            public void onLocationFetched(@NonNull LocationInfo location) {
                assertEquals((int)my_speed,(int)location.getSpeed());
            }

            @Override
            public void onError(@NonNull Exception e) {

            }
        });
    }

    @Test
    public void testTimezoneName(){
      locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
        @Override
        public void onLocationFetched(@NonNull LocationInfo location) {
          assertEquals(my_timezone, location.getTimezoneName());
        }

        @Override
        public void onError(@NonNull Exception e) {

        }
      });
    }

  @Test
  public void testTimezoneOffset(){
    locationProvider.fetchLocation(new LocationProvider.LocationFetchListener() {
      @Override
      public void onLocationFetched(@NonNull LocationInfo location) {
        assertEquals(my_timezone_offset, location.getTimezoneOffset());
      }

      @Override
      public void onError(@NonNull Exception e) {

      }
    });
  }
}
