package com.softweb.comsquare_android.providers_tests;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.telephony.TelephonyManager;

import com.softweb.comsquare_android.domain.phone_stats.DeviceInfo;
import com.softweb.comsquare_android.domain.providers.DeviceInfoProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DeviceInfoProviderTest {

    /* ----------------------------------------------------------
                Setup your device's information here
     */

    private static final String MY_DEVICE_OS_VERSION       = "Android 24"; // Replace 25 with your test device SDK int
    private static final String MY_DEVICE_BRAND            = "xiaomi"; // Device brand, usually is low case but no worries
    private static final String MY_DEVICE_NAME             = "mido"; // Device name, really strange value not related to brand or something
    private static final String APP_VERSION                = "1.0.0_dev"; // Current version of the application, in debug _dev suffix is required
    private static final boolean MY_DEVICE_IS_ROOTED       = false; // Device is rooted
    private static final boolean MY_DEVICE_IMEI_IS_VALID   = true; // Imei value is correctly configured, meaning that its value is not all zeros or empty

    // ----------------------------------------------------------

    private DeviceInfo mDeviceInfo;

    @Before
    public void setup(){

        // Setup device info provider here
        // Configure dependencies for device provider

        final Context context = InstrumentationRegistry.getTargetContext();
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        final PackageManager packageManager = context.getPackageManager();

        final DeviceInfoProvider deviceInfoProvider = new DeviceInfoProvider(telephonyManager,packageManager,context);

        mDeviceInfo = deviceInfoProvider.fetchDeviceInfo();
    }

    // ------------------------------- Unique values tests -------------------------------

    @Test
    public void testOsVersion(){
        assertEquals(MY_DEVICE_OS_VERSION,mDeviceInfo.getOsVersion());
    }

    @Test
    public void testDeviceBrand(){
        assertEquals(MY_DEVICE_BRAND.toLowerCase(),
                mDeviceInfo.getBrand().toLowerCase());
    }

    @Test
    public void testAppVersion(){
        assertEquals(APP_VERSION.toLowerCase(),
                mDeviceInfo.getAppVersion().toLowerCase());
    }

    @Test
    public void testDeviceIsRooted(){
        assertEquals(MY_DEVICE_IS_ROOTED,
                mDeviceInfo.getDeviceIsRooted());
    }

    @Test
    public void testImeiIsValid(){
        assertEquals(MY_DEVICE_IMEI_IS_VALID,
                mDeviceInfo.isImeiIsValid());
    }

    @Test
    public void testDeviceName(){
        assertEquals(MY_DEVICE_NAME.toLowerCase(),
                mDeviceInfo.getDevice());
    }

}
