package com.softweb.comsquare_android.providers_tests;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.softweb.comsquare_android.domain.phone_stats.NetworkInfo;
import com.softweb.comsquare_android.domain.providers.network.IpApiProvider;
import com.softweb.comsquare_android.domain.providers.network.IpProvider;
import com.softweb.comsquare_android.domain.providers.network.NetworkInfoProvider;
import com.softweb.comsquare_android.utils.AppExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import timber.log.Timber;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class NetworkInfoProviderTest {

  // ----------------- Provide the expected results here ------------------------------------------

  private static final String DEVICE_IP = "94.66.59.221"; // Proxy IP of the device. Currently the proxy ip is fetched from http://whatismyip.akamai.com/
  private static final String ISP_NAME = "OTEnet S.A.";

  // ----------------------------------------------------------------------------------------------

  private NetworkInfoProvider.Builder providerBuilder;
  private NetworkInfoProvider provider;

  @Before
  public void setup(){

    // Init dependencies
    providerBuilder = new NetworkInfoProvider.Builder();

    final Context context = InstrumentationRegistry.getTargetContext();
    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    final IpProvider ipApiProvider = new IpApiProvider();
    final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);

    providerBuilder
        .setpTelephonyManager(telephonyManager)
        .setIpProvider(ipApiProvider)
        .setSubscriptionManager(subscriptionManager)
        .setWifiManager(wifiManager);

    provider = providerBuilder.createNetworkInfoProvider();
  }

  @Test
  public void testIPAddress(){

    final NetworkInfo networkInfo = provider.fetchNetworkInfo();

    assertEquals(networkInfo.getIpAddress(), DEVICE_IP);
  }
  
}
