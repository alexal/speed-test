package com.softweb.comsquare_android.home_screen.interactor;

import android.os.Handler;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayer;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.utils.App;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Executor;

import timber.log.Timber;

public class DownloadTestInteractor extends AbstractInteractor {

  private final Contract.TestView testView;
  private final TestFilesRepository testFilesRepository;
  private final SpeedDisplayer speedDisplayer;
  private final String fileSizeID;
  private final boolean timeBasedTesting;
  private final Handler threadHandler;
  private final FileOutputStream fileOutputStream;
  private final File outputFile;
  private final UploadTestInteractor uploadTestInteractor; // Used to trigger the upload test after download completion

  // State
  private long testStartMillis;
  private long testEndMillis;
  private long testCurrentMillis;
  private boolean testRunning;

  public DownloadTestInteractor(Executor mainThread,
                                Executor executionThread,
                                TestFilesRepository testFilesRepository,
                                Contract.TestView testView,
                                SpeedDisplayer speedDisplayer,
                                String fileSizeID,
                                boolean timeBasedTesting,
                                FileOutputStream fileOutputStream,
                                File outputFile,
                                UploadTestInteractor uploadTestInteractor) {

    super(mainThread, executionThread);
    this.testView = testView;
    this.testFilesRepository = testFilesRepository;
    this.speedDisplayer = speedDisplayer;
    this.timeBasedTesting = timeBasedTesting;
    this.threadHandler = new Handler();
    this.fileOutputStream = fileOutputStream;
    if (timeBasedTesting)
      this.fileSizeID = TestFilesRepository.MB_1500;
    else
      this.fileSizeID = fileSizeID;
    this.outputFile = outputFile;
    this.uploadTestInteractor = uploadTestInteractor;
  }

  private void showCurrentSpeed(final long startMillis, final long currentMillis, final long currentDownloaded) {

    final String displayedResult = speedDisplayer.getDisplayedSpeed(startMillis, currentMillis, currentDownloaded);
    mainThread.execute(() -> testView.showDownloadResult(displayedResult));
  }

  private long computeCurrentProgress(final long downloaded,
                                      final long total,
                                      final long millisPassed){
    if (!timeBasedTesting)
      return (100 * downloaded) / total;
    else
      return (100 * millisPassed) / App.TEST_TIME_DURATION_MILLIS;
  }

  private void stopDownloadTestAndBeginUpload(){

    this.testRunning = false;
    testFilesRepository.cancelFetch();
    try {
      fileOutputStream.close();
    } catch (IOException e) {
      Timber.e(e);
    }

    mainThread.execute(() -> {
      testView.animateDownloading(false);
      testView.setDownloadProgress(100L);
    });

    uploadTestInteractor.execute();
  }

  @Override
  public void run() {

    mainThread.execute(() -> {
      testView.animateDownloading(true);
      testView.setStartButton(false);
    });

    final TestFilesRepository.DownloadProgressListener downloadProgressListener = (downloaded, total, bytes) -> {

      if (downloaded == total) { // Download completed

        this.testRunning = false;
        testEndMillis = System.currentTimeMillis();

        showCurrentSpeed(testStartMillis, testEndMillis, downloaded);
        stopDownloadTestAndBeginUpload();
        return;
      }

      if (downloaded == 0) {
        testStartMillis = System.currentTimeMillis();
        return;
      }

      try {
        fileOutputStream.write(bytes);
      } catch (IOException e) {
        Timber.e(e);
      }

      // Compute current progress and current speed

      if (testRunning){

        testCurrentMillis = System.currentTimeMillis();

        showCurrentSpeed(testStartMillis, testCurrentMillis, downloaded);

        final long progress = computeCurrentProgress(downloaded, total, testCurrentMillis - testStartMillis);

        mainThread.execute(() -> testView.setDownloadProgress(progress));
      }
    };

    if (timeBasedTesting) // Tell the thread handler to stop the test after the test time has passed
      threadHandler.postDelayed(this::stopDownloadTestAndBeginUpload, App.TEST_TIME_DURATION_MILLIS);

    this.testRunning = true;

    testFilesRepository.fetchTestFile(fileSizeID, downloadProgressListener);
  }
}
