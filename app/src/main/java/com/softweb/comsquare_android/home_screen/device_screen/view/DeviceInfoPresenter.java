package com.softweb.comsquare_android.home_screen.device_screen.view;

import android.os.Environment;

import com.softweb.comsquare_android.domain.ExportJSONHelper;
import com.softweb.comsquare_android.domain.phone_stats.ExportInfo;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoRefreshClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.ExportJSONInteractor;
import com.softweb.comsquare_android.utils.AppExecutors;

import java.io.File;

public class DeviceInfoPresenter {

  private Contract.DeviceInfoView mView;

  private final DeviceInfoClickInteractor deviceInfoClickInteractor;
  private final DeviceInfoRefreshClickInteractor deviceInfoRefreshClickInteractor;
  private final ExportInfoCache exportInfoCache;

  public DeviceInfoPresenter(DeviceInfoClickInteractor deviceInfoClickInteractor,
                             DeviceInfoRefreshClickInteractor deviceInfoRefreshClickInteractor,
                             ExportInfoCache infoFetchedHolderInteractor) {

    this.deviceInfoClickInteractor = deviceInfoClickInteractor;
    this.deviceInfoRefreshClickInteractor = deviceInfoRefreshClickInteractor;
    this.exportInfoCache = infoFetchedHolderInteractor;

  }

  void attachView(Contract.DeviceInfoView view){
    mView = view;
  }

  public void onStart(){
    deviceInfoClickInteractor.execute();
  }

  public void onStop(){}

  public void onRefreshClick(){
    deviceInfoRefreshClickInteractor.execute();
  }

  public void onExportClick() {

    // Because export data have a 'current' state, export interactor is dynamically configured

    final ExportInfo currentExportInfo = exportInfoCache.getCachedExportInfo();

    final ExportJSONHelper exportJSONHelper = new ExportJSONHelper(currentExportInfo);

    final File outputDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);

    final ExportJSONInteractor exportJSONInteractor = new ExportJSONInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        exportJSONHelper,
        mView,
        outputDirectory);

    exportJSONInteractor.execute();
  }
}
