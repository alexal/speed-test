package com.softweb.comsquare_android.home_screen.test_screen;

import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.home_screen.interactor.BeginTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DownloadTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.QuestionaireItemClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.TestStateInteractor;

public class TestsPresenter implements Contract.Presenter {

  private com.softweb.comsquare_android.home_screen.Contract.TestView mView;
  private final BeginTestInteractor beginTestInteractor;
  private final QuestionaireItemClickInteractor questionaireItemClickInteractor;
  private final TestStateInteractor testStateInteractor;
  private final DownloadTestInteractor downloadTestInteractor;

  public TestsPresenter(BeginTestInteractor beginTestInteractor,
                        QuestionaireItemClickInteractor questionaireItemClickInteractor,
                        TestStateInteractor testStateInteractor,
                        DownloadTestInteractor downloadTestInteractor) {

    this.beginTestInteractor = beginTestInteractor;
    this.questionaireItemClickInteractor = questionaireItemClickInteractor;
    this.testStateInteractor = testStateInteractor;
    this.downloadTestInteractor = downloadTestInteractor;
  }

  public void attachView(com.softweb.comsquare_android.home_screen.Contract.TestView view) {
    mView = view;
  }

  @Override
  public void onBeginTestClick() {
    beginTestInteractor.execute();
  }

  @Override
  public void onQuestionaireItemClick(TestEnvironmentType type) {

    testStateInteractor.setSelectedEnvironmentType(type); // Save the state

    questionaireItemClickInteractor.execute(); // Run the scenario after item click

    downloadTestInteractor.execute();
  }
}
