package com.softweb.comsquare_android.home_screen.interactor;

import android.os.Handler;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayer;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.Contract;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Executor;

import timber.log.Timber;

/**
 * Upload test use case class. The upload test currently is ONLY size-based, meaning
 * that it will be completed when the provided file is fully uploaded
 */
public class UploadTestInteractor extends AbstractInteractor {

  public static final int STATUS_UPLOADING = 420;
  public static final int STATUS_PAUSED = 421;
  public static final int STATUS_ERROR = 422;

  private final Contract.TestView testView;
  private final TestFilesRepository testFilesRepository;
  private final SpeedDisplayer speedDisplayer;
  private final FileInputStream fileInputStream;
  private final File file;
  private final Handler threadHandler;


  public UploadTestInteractor(Executor mainThread,
                              Executor executionThread,
                              Contract.TestView testView,
                              TestFilesRepository testFilesRepository,
                              SpeedDisplayer speedDisplayer,
                              FileInputStream fileInputStream,
                              File file) {

    super(mainThread, executionThread);
    this.testView = testView;
    this.testFilesRepository = testFilesRepository;
    this.speedDisplayer = speedDisplayer;
    this.fileInputStream = fileInputStream;
    this.threadHandler = new Handler();
    this.file = file;
  }

  @Override
  public void run() {

    mainThread.execute(() -> {
      testView.animateUploading(true);
    });

    final TestFilesRepository.UploadProgressListener progressListener =
        (uploaded, total, percent, speed) -> Timber.e("uploaded = %s, total = %s, percent = %s, speed = %s",uploaded, total, percent, speed);

    testFilesRepository.uploadTestFile(file, progressListener);

  }
}
