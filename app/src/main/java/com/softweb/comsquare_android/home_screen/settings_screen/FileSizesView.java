package com.softweb.comsquare_android.home_screen.settings_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.softweb.comsquare_android.utils.view.ListDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class FileSizesView extends ListDialogFragment {

  private static final String FILES_KEY = "files_sizes_key";

  private ArrayList<ItemView> filesSizesData;
  private SettingsPresenter settingsPresenter;

  public static FileSizesView newInstance(ArrayList<ItemView> fileSizes) {

    Bundle args = new Bundle();
    args.putParcelableArrayList(FILES_KEY, fileSizes);
    FileSizesView fragment = new FileSizesView();
    fragment.setArguments(args);
    return fragment;
  }

  public void injectPresenter(SettingsPresenter settingsPresenter){
    this.settingsPresenter = settingsPresenter;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    filesSizesData = getArguments().getParcelableArrayList(FILES_KEY);
  }

  @Override
  protected List<ItemView> getItems() {
    return filesSizesData;
  }

  @Override
  protected String getDialogTitle() {
    return "Available file sizes";
  }

  @Override
  protected ItemClickListener getClickListener() {
    return settingsPresenter::onFileSizeSelected;
  }
}
