package com.softweb.comsquare_android.home_screen.settings_screen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseFragmentView;
import com.softweb.comsquare_android.home_screen.Contract;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SettingsView extends BaseFragmentView implements Contract.SettingsView {

  public static final int SPEED_DISPLAY_UNIT_KB = 420;
  public static final int SPEED_DISPLAY_UNIT_MB = 421;

  public static final int SPEED_TESTING_SIZE_BASED = 430;
  public static final int SPEED_TESTING_TIME_BASED = 431;

  @BindView(R.id.toggle_settings_unit_kb)
  protected ToggleButton toggleSettingsUnitKb;

  @BindView(R.id.toggle_settings_unit_mb)
  protected ToggleButton toggleSettingsUnitMb;

  @BindView(R.id.toggle_settings_testing_size_based)
  protected ToggleButton toggleSettingsTestingSizeBased;

  @BindView(R.id.toggle_settings_testing_time_based)
  protected ToggleButton toggleSettingsTestingTimeBased;

  @BindView(R.id.tv_settings_file_size_selected)
  protected TextView tvSettingsFileSizeSelected;

  private SettingsPresenter mSettingsPresenter;

  public static SettingsView newInstance() {

    Bundle args = new Bundle();

    SettingsView fragment = new SettingsView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutId() {
    return R.layout.frag_view_settings;
  }

  @Override
  protected void initViews(View root) {

    mSettingsPresenter.attachView(this);
    mSettingsPresenter.onAttach();

    tvSettingsFileSizeSelected.setOnClickListener(view -> mSettingsPresenter.onFileSizeClick());

    toggleSettingsUnitKb.setOnClickListener(view -> mSettingsPresenter.onDisplayUnitSelected(SPEED_DISPLAY_UNIT_KB));
    toggleSettingsUnitMb.setOnClickListener(view -> mSettingsPresenter.onDisplayUnitSelected(SPEED_DISPLAY_UNIT_MB));

    toggleSettingsTestingTimeBased.setOnClickListener(view -> mSettingsPresenter.onTestingSelected(SPEED_TESTING_TIME_BASED));
    toggleSettingsTestingSizeBased.setOnClickListener(view -> mSettingsPresenter.onTestingSelected(SPEED_TESTING_SIZE_BASED));

  }

  @Override
  public void injectPresenter(SettingsPresenter presenter) {
    mSettingsPresenter = presenter;
  }

  @Override
  public void showFileSizesView(FileSizesView view) {

    view.injectPresenter(mSettingsPresenter);

    requireFragmentManager().beginTransaction()
        .add(view, "FileSizesView")
        .commit();
  }

  @Override
  public void hideFileSizesView() {
    requireFragmentManager().beginTransaction()
        .remove(
            requireFragmentManager().findFragmentByTag("FileSizesView")
        )
        .commit();
  }

  @Override
  public void setFileSizeText(String fileSizeText) {
    tvSettingsFileSizeSelected.setText(fileSizeText);
  }

  @Override
  public void setSpeedDisplayUnit(int displayUnit) {

    if (displayUnit == SPEED_DISPLAY_UNIT_KB){
      toggleSettingsUnitKb.setChecked(true);
      toggleSettingsUnitMb.setChecked(false);
    }
    else if (displayUnit == SPEED_DISPLAY_UNIT_MB){
      toggleSettingsUnitKb.setChecked(false);
      toggleSettingsUnitMb.setChecked(true);
    }
//    else
//      throw new IllegalArgumentException("Must be one of SPEED_DISPLAY_UNIT values");
  }

  @Override
  public void setSpeedTesting(int testing) {

    if (testing == SPEED_TESTING_SIZE_BASED){
      toggleSettingsTestingSizeBased.setChecked(true);
      toggleSettingsTestingTimeBased.setChecked(false);
    }
    else if (testing == SPEED_TESTING_TIME_BASED){
      toggleSettingsTestingSizeBased.setChecked(false);
      toggleSettingsTestingTimeBased.setChecked(true);
    }
//    else
//      throw new IllegalArgumentException("Must be one of SPEED_TESTING values");
  }

  @Override
  public void setFileSizeOptionEnabled(boolean enabled) {
      if (enabled){
        tvSettingsFileSizeSelected.setAlpha(1.0f);
        tvSettingsFileSizeSelected.setOnClickListener(view -> mSettingsPresenter.onFileSizeClick());
      }
      else {
        tvSettingsFileSizeSelected.setAlpha(0.3f);
        tvSettingsFileSizeSelected.setOnClickListener(null);

      }

  }

}
