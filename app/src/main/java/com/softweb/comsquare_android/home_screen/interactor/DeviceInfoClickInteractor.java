package com.softweb.comsquare_android.home_screen.interactor;

import android.support.annotation.NonNull;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.phone_stats.DeviceInfo;
import com.softweb.comsquare_android.domain.phone_stats.ExportInfo;
import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;
import com.softweb.comsquare_android.domain.phone_stats.NetworkInfo;
import com.softweb.comsquare_android.domain.phone_stats.RadioInfo;
import com.softweb.comsquare_android.domain.providers.DeviceInfoProvider;
import com.softweb.comsquare_android.domain.providers.RadioInfoProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationProvider;
import com.softweb.comsquare_android.domain.providers.network.NetworkInfoProvider;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.device_screen.view.ExportInfoCache;
import com.softweb.comsquare_android.home_screen.device_screen.view.InfoViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class DeviceInfoClickInteractor extends AbstractInteractor implements LocationProvider.LocationFetchListener{

  private final Contract.DeviceInfoView view;
  private final RadioInfoProvider radioInfoProvider;
  private final NetworkInfoProvider networkInfoProvider;
  private final DeviceInfoProvider deviceInfoProvider;
  private final LocationProvider locationProvider;
  private final ExportInfoCache exportInfoCache;

  private RadioInfo radioInfo;
  private NetworkInfo networkInfo;
  private DeviceInfo deviceInfo;
  private LocationInfo locationInfo;

  public DeviceInfoClickInteractor(Executor mainThread,
                                   Executor executionThread,
                                   Contract.DeviceInfoView view,
                                   RadioInfoProvider radioInfoProvider,
                                   DeviceInfoProvider deviceInfoProvider,
                                   NetworkInfoProvider networkInfoProvider,
                                   LocationProvider locationProvider,
                                   ExportInfoCache exportInfoCache){

    super(mainThread, executionThread);

    this.view = view;
    this.radioInfoProvider = radioInfoProvider;
    this.networkInfoProvider = networkInfoProvider;
    this.deviceInfoProvider = deviceInfoProvider;
    this.locationProvider = locationProvider;
    this.exportInfoCache = exportInfoCache;

  }

  private List<InfoViewModel> parseRadioInfo(RadioInfo radioInfo){

    List<InfoViewModel> infoViewModelList = new ArrayList<>();

    final InfoViewModel wifiSSID = new InfoViewModel("Wifi SSID:",radioInfo.getWifiSsid());
    final InfoViewModel level = new InfoViewModel("Level:", String.valueOf(radioInfo.getLevel()));
    final InfoViewModel wifiFrequencyBand = new InfoViewModel("Wifi frequency band:",radioInfo.getWifi_frequency_band_mhz());

    infoViewModelList.add(wifiSSID);
    infoViewModelList.add(level);
    infoViewModelList.add(wifiFrequencyBand);

    return infoViewModelList;
  }

  private List<InfoViewModel> parseDeviceInfo(DeviceInfo deviceInfo){

    List<InfoViewModel> result = new ArrayList<>();

    final InfoViewModel appSoftwareVersion = new InfoViewModel("App Version:", deviceInfo.getAppVersion());

    result.add(appSoftwareVersion);

    return result;
  }

  private List<InfoViewModel> parseNetworkInfo(NetworkInfo networkInfo){

    List<InfoViewModel> result = new ArrayList<>();

    final InfoViewModel networkOperator = new InfoViewModel("Serving network operator:", networkInfo.getServiceNetworkOperator());
    final InfoViewModel homeNetworkOperator = new InfoViewModel("Home network operator:", networkInfo.getHomeNetworkOperator());

    result.add(networkOperator);
    result.add(homeNetworkOperator);

    return result;
  }

  private ExportInfo getCurrentExportInfo(){
    return new ExportInfo(deviceInfo, radioInfo, networkInfo, locationInfo);
  }

  @Override
  public void run() {

    // Show loading first

    mainThread.execute(() -> {
      view.setLoading(true);
      view.setExportButtonEnabled(false);
    });

    // Fetch all the values to be displayed and cache them, location is the most 'costly'
    // so the caching happens when location value is fetched

    radioInfo = radioInfoProvider.fetchRadioInfo();
    networkInfo = networkInfoProvider.fetchNetworkInfo();
    deviceInfo = deviceInfoProvider.fetchDeviceInfo();

    locationProvider.fetchLocation(this);

    // Parse them to view model

    List<InfoViewModel> radioInfoViewModel = parseRadioInfo(radioInfo);
    List<InfoViewModel> networkInfoViewModel = parseNetworkInfo(networkInfo);
    List<InfoViewModel> deviceInfoViewModel = parseDeviceInfo(deviceInfo);

    // Show them on UI thread of course

    mainThread.execute(() -> {
      view.setLoading(false);
      view.renderViewModel(Contract.DeviceInfoView.RADIO_INFO, radioInfoViewModel);
      view.renderViewModel(Contract.DeviceInfoView.NETWORK_INFO, networkInfoViewModel);
      view.renderViewModel(Contract.DeviceInfoView.DEVICE_INFO, deviceInfoViewModel);
    });
  }

  private void dispatchLocationFetch(@NonNull LocationInfo locationInfo) {

    this.locationInfo = locationInfo;
    final ExportInfo currentExportInfo = getCurrentExportInfo();

    exportInfoCache.cacheExportInfo(currentExportInfo);

    mainThread.execute(() -> view.setExportButtonEnabled(true));
  }

  @Override
  public void onLocationFetched(@NonNull LocationInfo location) {
    dispatchLocationFetch(location);
  }

  @Override
  public void onError(@NonNull Exception e) {

  }
}
