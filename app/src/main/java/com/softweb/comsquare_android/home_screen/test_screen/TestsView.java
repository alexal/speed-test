package com.softweb.comsquare_android.home_screen.test_screen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseFragmentView;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.utils.AnimationUtil;

import butterknife.BindView;

public class TestsView extends BaseFragmentView implements Contract.TestView {

  @BindView(R.id.progress_download)
  protected ProgressBar progressDownload;

  @BindView(R.id.iv_test_download)
  protected ImageView ivTestDownload;

  @BindView(R.id.progress_upload)
  protected ProgressBar progressUpload;

  @BindView(R.id.iv_test_upload)
  protected ImageView ivTestUpload;

  @BindView(R.id.tv_test_download_result)
  protected TextView tvTestDownloadResult;

  @BindView(R.id.tv_test_download_progress)
  protected TextView tvTestDownloadProgress;

  @BindView(R.id.tv_test_upload_result)
  protected TextView tvTestUploadResult;

  @BindView(R.id.tv_test_ping_value)
  protected TextView tvTestPing;

  @BindView(R.id.btn_test_start)
  protected Button btnTestStart;

  private TestsPresenter mPresenter;

  public static TestsView newInstance() {

    Bundle args = new Bundle();

    TestsView fragment = new TestsView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutId() {
    return R.layout.frag_view_test;
  }

  @Override
  protected void initViews(View root) {

    // Presenter setup

    mPresenter.attachView(this);

    progressDownload.setProgress(0);
    progressUpload.setProgress(0);

    btnTestStart.setOnClickListener(view -> mPresenter.onBeginTestClick());
  }

  @Override
  public void injectPresenter(TestsPresenter presenter) {
    mPresenter = presenter;
  }


  public void showQuestionaireView(QuestionaireView questionaireView) {

    questionaireView.injectPresenter(mPresenter);

    requireFragmentManager().beginTransaction()
        .add(questionaireView,"w")
        .commit();
  }

  @Override
  public void showDownloadResult(String result) {
    tvTestDownloadResult.setText(result);
  }

  @Override
  public void showMessage(String message) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void setDownloadProgress(long progress) {
    tvTestDownloadProgress.setText(progress + "%");
    progressDownload.setProgress((int) progress, true);
  }

  @Override
  public void setUploadProgress(long progress) {
    progressUpload.setProgress((int) progress);
  }

  @Override
  public void setPingProgress(int progress) {

  }

  @Override
  public void animateDownloading(boolean enabled) {
    if (enabled){
      ivTestDownload.setVisibility(View.VISIBLE);
      AnimationUtil.animateBlink(ivTestDownload);
    }
    else{
      ivTestDownload.setVisibility(View.GONE);
      AnimationUtil.stopBlinkAnimation(ivTestDownload);
    }
  }

  @Override
  public void animateUploading(boolean enabled) {
    if (enabled)
      AnimationUtil.animateBlink(ivTestUpload);
    else
      AnimationUtil.stopBlinkAnimation(ivTestUpload);
  }

  @Override
  public void setStartButton(boolean enabled) {
    btnTestStart.setEnabled(enabled);
  }
}


