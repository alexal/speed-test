package com.softweb.comsquare_android.home_screen.history_screen;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseFragmentView;

import butterknife.BindView;

public class HistoryView extends BaseFragmentView implements Contract.View {

    @BindView(R.id.tv_history_message_view)
    protected TextView tvHistoryMessageView;

    @BindView(R.id.rv_history)
    protected RecyclerView rvHistory;

    private HistoryPresenter mPresenter;

    public static HistoryView newInstance() {

        Bundle args = new Bundle();

        HistoryView fragment = new HistoryView();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_view_history;
    }

    @Override
    protected void initViews(View root) {

        // Attach the presenter
        mPresenter.attachView(this);
        mPresenter.onAttach();
    }

    @Override
    public void injectPresenter(HistoryPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showHistoryResults() {
        tvHistoryMessageView.setText("I will show you one day history results here :)");
    }

}
