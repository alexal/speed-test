package com.softweb.comsquare_android.home_screen.interactor;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.domain.TestStateHolder;
import com.softweb.comsquare_android.utils.AppExecutors;

import java.util.concurrent.Executor;

import timber.log.Timber;

/**
 * Interactor class to handle the state of one test.
 * It follows the singleton pattern because we want to keep it always alive, it is a state
 * holder object and it makes sense to do so.
 */
public class TestStateInteractor extends AbstractInteractor {

  private final TestStateHolder testStateHolder;
  private static TestStateInteractor INSTANCE;

  public static TestStateInteractor getInstance() {
    if (INSTANCE == null)
      INSTANCE = new TestStateInteractor(
          AppExecutors.getInstance().mainThread(),
          AppExecutors.getInstance().diskIO(),
          new TestStateHolder());

    return INSTANCE;

  }

  private TestStateInteractor(Executor mainThread,
                              Executor executionThread,
                              TestStateHolder testStateHolder) {

    super(mainThread, executionThread);
    this.testStateHolder = testStateHolder;
  }

  public void setSelectedEnvironmentType(TestEnvironmentType type){
    Timber.d("Setting selected environment type: %s", type.getEnvironmentType());
    executionThread.execute(() -> testStateHolder.setTestEnvironmentType(type));
  }

  @Override
  public void run() {

    // This interactor does not execute something directly, only keeping the necessary state


  }
}
