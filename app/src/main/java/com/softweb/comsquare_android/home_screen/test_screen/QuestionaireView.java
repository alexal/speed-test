package com.softweb.comsquare_android.home_screen.test_screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseDialogFragmentView;
import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.home_screen.interactor.QuestionaireItemClickInteractor;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QuestionaireView extends BaseDialogFragmentView implements Contract.QuestionaireView {

  private static final String TYPES_LIST_KEY = "types_list_key";

  @BindView(R.id.rv_questionaire_types)
  protected RecyclerView rvQuestionaireTypes;

  private Contract.Presenter presenter;

  public static QuestionaireView newInstance(@NonNull ArrayList<TestEnvironmentType> typesList) {

    Bundle args = new Bundle();
    args.putParcelableArrayList(TYPES_LIST_KEY, typesList);
    QuestionaireView fragment = new QuestionaireView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutId() {
    return R.layout.frag_view_questionnaire;
  }

  @Override
  protected void initViews(View root) {

    final ArrayList<TestEnvironmentType> typesToRender = getArguments().getParcelableArrayList(TYPES_LIST_KEY);
    final EnvironmentTypesAdapter adapter = new EnvironmentTypesAdapter(typesToRender, new EnvironmentTypesAdapter.TypeClickListener() {
      @Override
      public void onItemClick(TestEnvironmentType clickedType) {
        presenter.onQuestionaireItemClick(clickedType);
      }
    });
    final LinearLayoutManager llm = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
    rvQuestionaireTypes.setLayoutManager(llm);
    rvQuestionaireTypes.setAdapter(adapter);

  }

  @Override
  public void injectPresenter(TestsPresenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setVisibility(boolean shown) {
    if (shown)
      requireFragmentManager()
          .beginTransaction()
          .add(this, getViewTag())
          .commit();
    else
      this.dismiss();
  }

}
