package com.softweb.comsquare_android.home_screen.device_screen.view;

import com.softweb.comsquare_android.domain.phone_stats.ExportInfo;

public class ExportInfoCache {

  private static ExportInfoCache INSTANCE;

  public static ExportInfoCache getINSTANCE(){
    if (INSTANCE == null)
      INSTANCE = new ExportInfoCache();
    return INSTANCE;
  }

  private ExportInfoCache(){}

  private ExportInfo exportInfo;

  public void cacheExportInfo(ExportInfo exportInfo) {
    this.exportInfo = exportInfo;
  }

  public ExportInfo getCachedExportInfo() {
    return exportInfo;
  }

}
