package com.softweb.comsquare_android.home_screen;

import android.support.annotation.IntDef;

import com.softweb.comsquare_android.home_screen.device_screen.view.DeviceInfoPresenter;
import com.softweb.comsquare_android.home_screen.device_screen.view.InfoViewModel;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoClickInteractor;
import com.softweb.comsquare_android.home_screen.settings_screen.FileSizesView;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsPresenter;
import com.softweb.comsquare_android.home_screen.test_screen.QuestionaireView;
import com.softweb.comsquare_android.home_screen.test_screen.TestsPresenter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

/**
 * Top level contract of home screen. Contains contracts for each subview of the screen.
 */
public interface Contract {

  /**
   *
   */
  interface DeviceInfoView{

    // Instead of enum use this super cool int def

    int DEVICE_INFO = 33;
    int NETWORK_INFO = 34;
    int RADIO_INFO = 35;
    @IntDef({DEVICE_INFO,NETWORK_INFO,RADIO_INFO})
    @Retention(RetentionPolicy.SOURCE)
    @interface Info {}

    void injectDevicePresenter(DeviceInfoPresenter presenter);

    void setExportButtonEnabled(boolean enabled);
    void showMessage(String message);
    void showActionMessage(String message);
    void setLoading(boolean show);
    void renderViewModel(@Info int InfoID, List<InfoViewModel> infoList);

  }

  interface TestView {
    void injectPresenter(TestsPresenter presenter);

    void showQuestionaireView(QuestionaireView questionaireView);

    void showDownloadResult(String result);
    void showMessage(String message);
    void setDownloadProgress(long progress);
    void setUploadProgress(long progress);
    void setPingProgress(int progress);
    void animateDownloading(boolean enabled);
    void animateUploading(boolean enabled);
    void setStartButton(boolean enabled);
  }

  interface HistoryView {

  }

  interface SettingsView {
    void injectPresenter(SettingsPresenter presenter);

    void showFileSizesView(FileSizesView view);
    void hideFileSizesView();
    void setFileSizeText(String fileSizeText);
    void setSpeedDisplayUnit(int displayUnit);
    void setSpeedTesting(int testing);
    void setFileSizeOptionEnabled(boolean enabled);

  }

}
