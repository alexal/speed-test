package com.softweb.comsquare_android.home_screen.device_screen.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softweb.comsquare_android.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoViewModelAdapter extends RecyclerView.Adapter<InfoViewModelAdapter.ViewHolder> {

  private final List<InfoViewModel> infoViewModels;

  public InfoViewModelAdapter(List<InfoViewModel> infoViewModels) {
    this.infoViewModels = new ArrayList<>(infoViewModels);
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device_info, parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.valueView.setText(infoViewModels.get(position).getInfoValue());
    holder.descriptionView.setText(infoViewModels.get(position).getInfoDescription());
  }

  @Override
  public int getItemCount() {
    return infoViewModels.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.tv_item_info_description)
    TextView descriptionView;

    @BindView(R.id.tv_item_info_value)
    TextView valueView;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
