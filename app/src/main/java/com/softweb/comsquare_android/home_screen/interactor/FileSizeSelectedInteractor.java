package com.softweb.comsquare_android.home_screen.interactor;

import android.content.SharedPreferences;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.settings_screen.FileSizesView;
import com.softweb.comsquare_android.utils.PrefHelper;
import com.softweb.comsquare_android.utils.view.ListDialogFragment;

import java.util.concurrent.Executor;

public class FileSizeSelectedInteractor extends AbstractInteractor {

  private final SharedPreferences.Editor editor;
  private ListDialogFragment.ItemView fileSizeId;
  private final Contract.SettingsView settingsView;

  public FileSizeSelectedInteractor(Executor mainThread,
                                       Executor executionThread,
                                       SharedPreferences.Editor editor,
                                       Contract.SettingsView settingsView) {
    super(mainThread, executionThread);
    this.editor = editor;
    this.settingsView = settingsView;
  }

  public void setFileSizeId(ListDialogFragment.ItemView fileSizeId) {
    this.fileSizeId = fileSizeId;
  }

  @Override
  public void run() {

    final String selectedFileSize = fileSizeId.getItemValue();

    editor.putString(PrefHelper.TEST_FILE_SIZE, selectedFileSize).apply();

    mainThread.execute(() -> {
      settingsView.hideFileSizesView();
      settingsView.setFileSizeText(selectedFileSize);
    });
  }
}
