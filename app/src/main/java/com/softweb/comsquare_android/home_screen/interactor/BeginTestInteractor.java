package com.softweb.comsquare_android.home_screen.interactor;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.home_screen.test_screen.Contract;
import com.softweb.comsquare_android.home_screen.test_screen.QuestionaireView;
import com.softweb.comsquare_android.home_screen.test_screen.TestsView;

import java.util.concurrent.Executor;

public class BeginTestInteractor extends AbstractInteractor {

  private final QuestionaireView questionaireView;
  private final TestsView testView;

  public BeginTestInteractor(Executor mainThread,
                             Executor executionThread,
                             final QuestionaireView questionaireView,
                             final TestsView testView) {

    super(mainThread, executionThread);
    this.questionaireView = questionaireView;
    this.testView = testView;
  }

  @Override
  public void run() {

    // At beginning of the test, currently just show the questionaire

    mainThread.execute(() -> testView.showQuestionaireView(questionaireView));
  }
}
