package com.softweb.comsquare_android.home_screen.interactor;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.home_screen.test_screen.Contract;

import java.util.concurrent.Executor;

public class QuestionaireItemClickInteractor extends AbstractInteractor {

  private final Contract.QuestionaireView questionaireView;

  public QuestionaireItemClickInteractor(Executor mainThread,
                                            Executor executionThread,
                                            Contract.QuestionaireView questionaireView) {
    super(mainThread, executionThread);
    this.questionaireView = questionaireView;
  }

  @Override
  public void run() {
    mainThread.execute(() -> {
      questionaireView.setVisibility(false);

      });
  }
}
