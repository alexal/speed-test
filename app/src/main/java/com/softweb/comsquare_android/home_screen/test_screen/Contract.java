package com.softweb.comsquare_android.home_screen.test_screen;

import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.home_screen.interactor.QuestionaireItemClickInteractor;

public interface Contract {

  interface QuestionaireView{
    void injectPresenter(TestsPresenter presenter);
    void setVisibility(boolean shown);
  }

  interface Presenter {

    void onBeginTestClick();
    void onQuestionaireItemClick(TestEnvironmentType type);

  }

}
