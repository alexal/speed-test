package com.softweb.comsquare_android.home_screen.interactor;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.home_screen.Contract;

import java.util.concurrent.Executor;

public class DeviceInfoRefreshClickInteractor extends AbstractInteractor {

  private final Contract.DeviceInfoView mView;
  private final DeviceInfoClickInteractor clickInteractor;

  public DeviceInfoRefreshClickInteractor(Executor mainThread,
                                          Executor executionThread,
                                          Contract.DeviceInfoView deviceInfoView,
                                          DeviceInfoClickInteractor clickInteractor) {
    super(mainThread, executionThread);
    mView = deviceInfoView;
    this.clickInteractor = clickInteractor;
  }

  @Override
  public void run() {
    mainThread.execute(clickInteractor::execute);
  }
}
