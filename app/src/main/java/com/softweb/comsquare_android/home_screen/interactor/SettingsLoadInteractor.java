package com.softweb.comsquare_android.home_screen.interactor;

import android.content.SharedPreferences;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsView;
import com.softweb.comsquare_android.utils.PrefHelper;

import java.util.concurrent.Executor;

public class SettingsLoadInteractor extends AbstractInteractor {

  private final SharedPreferences sharedPreferences;
  private final Contract.SettingsView settingsView;

  public SettingsLoadInteractor(Executor mainThread,
                                   Executor executionThread,
                                   SharedPreferences sharedPreferences,
                                   Contract.SettingsView settingsView) {
    super(mainThread, executionThread);
    this.settingsView = settingsView;
    this.sharedPreferences = sharedPreferences;
  }

  @Override
  public void run() {

    // Fetch all the settings stored in preferences
    final int speedDisplayUnit = sharedPreferences.getInt(PrefHelper.SPEED_DISPLAY_UNIT, SettingsView.SPEED_DISPLAY_UNIT_KB);
    final int speedTesting = sharedPreferences.getInt(PrefHelper.SPEED_TESTING, SettingsView.SPEED_TESTING_SIZE_BASED);
    final boolean fileSizeOptionEnabled = speedTesting == SettingsView.SPEED_TESTING_SIZE_BASED;
    final String fileSize;
    if (fileSizeOptionEnabled)
      fileSize = sharedPreferences.getString(PrefHelper.TEST_FILE_SIZE, TestFilesRepository.MB_50);
    else
      fileSize = TestFilesRepository.MB_1500;

    mainThread.execute(() -> {
      settingsView.setFileSizeText(fileSize);
      settingsView.setSpeedDisplayUnit(speedDisplayUnit);
      settingsView.setSpeedTesting(speedTesting);
      settingsView.setFileSizeOptionEnabled(fileSizeOptionEnabled);
    });
  }
}
