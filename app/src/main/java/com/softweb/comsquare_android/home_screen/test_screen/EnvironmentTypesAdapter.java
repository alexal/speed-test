package com.softweb.comsquare_android.home_screen.test_screen;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.domain.TestEnvironmentType;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentTypesAdapter extends RecyclerView.Adapter<EnvironmentTypesAdapter.TypeViewHolder>{

  public interface TypeClickListener{
    void onItemClick(TestEnvironmentType clickedType);
  }

  private final List<TestEnvironmentType> types;
  private final TypeClickListener typeClickListener;

  EnvironmentTypesAdapter(List<TestEnvironmentType> types, @NonNull TypeClickListener typeClickListener) {
    this.types = new ArrayList<>(types);
    this.typeClickListener = typeClickListener;
  }

  @NonNull
  @Override
  public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new TypeViewHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_questionaire_env_type,parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull TypeViewHolder holder, int position) {
    holder.bind(types.get(position), typeClickListener);
  }

  @Override
  public int getItemCount() {
    return types.size();
  }

  static class TypeViewHolder extends RecyclerView.ViewHolder{

    TextView typeValueView;
    ImageView typeIcon;
    ConstraintLayout rootView;

    TypeViewHolder(View itemView) {
      super(itemView);
      rootView = itemView.findViewById(R.id.cl_questionaire_root);
      typeValueView = itemView.findViewById(R.id.tv_questionaire_item_value);
      typeIcon = itemView.findViewById(R.id.iv_questionaire_item_icon);
    }

    void bind(final TestEnvironmentType item, final TypeClickListener clickListener){
      typeValueView.setText(item.getEnvironmentType());
      typeIcon.setImageResource(item.getResourceID());
      rootView.setOnClickListener(view -> clickListener.onItemClick(item));
    }
  }

}
