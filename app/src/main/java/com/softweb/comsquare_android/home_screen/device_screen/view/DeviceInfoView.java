package com.softweb.comsquare_android.home_screen.device_screen.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseFragmentView;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoClickInteractor;

import java.util.List;

import butterknife.BindView;

public class DeviceInfoView extends BaseFragmentView implements Contract.DeviceInfoView {

  @BindView(R.id.cl_device_root)
  protected ScrollView rootView;

  @BindView(R.id.rv_device_info_device)
  protected RecyclerView rvDeviceInfoDevice;

  @BindView(R.id.rv_device_info_network)
  protected RecyclerView rvDeviceInfoNetwork;

  @BindView(R.id.rv_device_info_radio)
  protected RecyclerView rvDeviceInfoRadio;

  @BindView(R.id.progress_device_info)
  protected ProgressBar progressBar;

  private DeviceInfoPresenter presenter;
  private boolean exportButtonEnabled = false;

  public static DeviceInfoView newInstance() {

    Bundle args = new Bundle();

    DeviceInfoView fragment = new DeviceInfoView();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected int getLayoutId() {
    return R.layout.frag_device_info;
  }

  @Nullable
  @Override
  protected String getViewTag() {
    return "DeviceInfoView";
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_toolbar_device_screen, menu);

    MenuItem exportItem = menu.findItem(R.id.menu_device_export);
    exportItem.setVisible(exportButtonEnabled);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    if (item.getItemId() == R.id.menu_device_refresh){
      presenter.onRefreshClick();
      return true;
    }
    if (item.getItemId() == R.id.menu_device_export){
      presenter.onExportClick();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void initViews(View root) {
    setHasOptionsMenu(true);
    presenter.attachView(this);
    presenter.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
    presenter.onStop();
  }

  @Nullable
  private RecyclerView getRecyclerViewById(@Info int infoID){
    switch (infoID){
      case DEVICE_INFO:
        return rvDeviceInfoDevice;
      case RADIO_INFO:
        return rvDeviceInfoRadio;
      case NETWORK_INFO:
        return rvDeviceInfoNetwork;
    }
    return null;
  }

  private void configureRv(int infoID,
                           List<InfoViewModel> infoViewModels){

    LinearLayoutManager llm = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
    final InfoViewModelAdapter adapter = new InfoViewModelAdapter(infoViewModels);
    final RecyclerView recyclerView = getRecyclerViewById(infoID);

    recyclerView.setLayoutManager(llm);
    recyclerView.setAdapter(adapter);
  }

  @Override
  public void injectDevicePresenter(DeviceInfoPresenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setExportButtonEnabled(boolean enabled) {
    exportButtonEnabled = enabled;
    requireActivity().invalidateOptionsMenu();
  }

  @Override
  public void showMessage(String message) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void showActionMessage(String message) {

    Snackbar snackbar = Snackbar.make(requireActivity().findViewById(R.id.bnv_home),
        message, Snackbar.LENGTH_LONG);

    final View.OnClickListener filesClickListener = view -> {
      Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
      fileIntent.setType("file/*");
      requireActivity().startActivity(fileIntent);
    };

    snackbar.setAction(R.string.files, filesClickListener);
    snackbar.show();
  }

  @Override
  public void setLoading(boolean show) {
    progressBar.setVisibility(getVisibility(show));
  }

  @Override
  public void renderViewModel(@Info int infoID, List<InfoViewModel> infoList) {
    configureRv(infoID, infoList);
  }
}
