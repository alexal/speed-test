package com.softweb.comsquare_android.home_screen.history_screen;

public interface Contract {

    interface View {
        void injectPresenter(HistoryPresenter presenter);

        void showHistoryResults();
    }

}
