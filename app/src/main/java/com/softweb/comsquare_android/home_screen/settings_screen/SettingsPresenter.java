package com.softweb.comsquare_android.home_screen.settings_screen;

import com.softweb.comsquare_android.base.AbstractPresenter;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.interactor.DisplayUnitSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizeSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizesClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SettingsLoadInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SpeedTestingValueSelectedInteractor;
import com.softweb.comsquare_android.utils.view.ListDialogFragment;

public class SettingsPresenter implements AbstractPresenter {

  private Contract.SettingsView settingsView;
  private final FileSizesClickInteractor fileSizesClickInteractor;
  private final FileSizeSelectedInteractor fileSizeSelectedInteractor;
  private final SettingsLoadInteractor settingsLoadInteractor;
  private final DisplayUnitSelectedInteractor displayUnitSelectedInteractor;
  private final SpeedTestingValueSelectedInteractor speedTestingValueSelectedInteractor;

  public SettingsPresenter(FileSizesClickInteractor fileSizesClickInteractor,
                           FileSizeSelectedInteractor fileSizeSelectedInteractor,
                           SettingsLoadInteractor settingsLoadInteractor,
                           DisplayUnitSelectedInteractor displayUnitSelectedInteractor,
                           SpeedTestingValueSelectedInteractor speedTestingValueSelectedInteractor) {

    this.fileSizesClickInteractor = fileSizesClickInteractor;
    this.fileSizeSelectedInteractor = fileSizeSelectedInteractor;
    this.settingsLoadInteractor = settingsLoadInteractor;
    this.displayUnitSelectedInteractor = displayUnitSelectedInteractor;
    this.speedTestingValueSelectedInteractor = speedTestingValueSelectedInteractor;
  }

  public void attachView(Contract.SettingsView settingsView) {
    this.settingsView = settingsView;
  }

  @Override
  public void onAttach() {
    settingsLoadInteractor.execute();
  }

  @Override
  public void onDetach() {

  }

  void onFileSizeClick(){
    fileSizesClickInteractor.execute();
  }

  void onFileSizeSelected(ListDialogFragment.ItemView itemSize){
    fileSizeSelectedInteractor.setFileSizeId(itemSize);
    fileSizeSelectedInteractor.execute();
  }

  void onDisplayUnitSelected(int selected){
    displayUnitSelectedInteractor.setSelectedDisplayUnit(selected);
    displayUnitSelectedInteractor.execute();
  }

  void onTestingSelected(int selected){
    speedTestingValueSelectedInteractor.setSelectedSpeedTestingMethod(selected);
    speedTestingValueSelectedInteractor.execute();
  }
}
