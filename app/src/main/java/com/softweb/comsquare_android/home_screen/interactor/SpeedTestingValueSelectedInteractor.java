package com.softweb.comsquare_android.home_screen.interactor;

import android.content.SharedPreferences;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsView;
import com.softweb.comsquare_android.utils.PrefHelper;

import java.util.concurrent.Executor;

public class SpeedTestingValueSelectedInteractor extends AbstractInteractor {

  private final Contract.SettingsView settingsView;
  private final SharedPreferences.Editor editor;
  private int selectedTestingMethod;

  public SpeedTestingValueSelectedInteractor(Executor mainThread,
                                       Executor executionThread,
                                       Contract.SettingsView settingsView,
                                       SharedPreferences.Editor editor) {
    super(mainThread, executionThread);
    this.settingsView = settingsView;
    this.editor = editor;
  }

  public void setSelectedSpeedTestingMethod(int speedTestingMethod) {
    this.selectedTestingMethod = speedTestingMethod;
  }

  @Override
  public void run() {

    editor.putInt(PrefHelper.SPEED_TESTING, selectedTestingMethod).commit();

    final boolean disableFileSizeOption = selectedTestingMethod == SettingsView.SPEED_TESTING_TIME_BASED;

    mainThread.execute(() -> {
      settingsView.setSpeedTesting(selectedTestingMethod);
      settingsView.setFileSizeOptionEnabled(!disableFileSizeOption);
      if (disableFileSizeOption)
        settingsView.setFileSizeText(
            TestFilesRepository.MB_1500);
    });
  }
}
