package com.softweb.comsquare_android.home_screen.device_screen.view;

public final class InfoViewModel {

  private final String infoDescription;
  private final String infoValue;

  public InfoViewModel(String infoDescription, String infoValue) {
    this.infoDescription = infoDescription;
    this.infoValue = infoValue;
  }

  public String getInfoDescription() {
    return infoDescription;
  }

  public String getInfoValue() {
    return infoValue;
  }
}
