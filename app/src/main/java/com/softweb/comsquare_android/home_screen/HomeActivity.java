package com.softweb.comsquare_android.home_screen;

import android.Manifest;
import android.content.Context;
import android.location.Geocoder;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.google.android.gms.location.LocationServices;
import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseActivity;
import com.softweb.comsquare_android.domain.TestEnvironmentProvider;
import com.softweb.comsquare_android.domain.TestEnvironmentType;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayer;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayerKb;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayerMb;
import com.softweb.comsquare_android.domain.providers.DeviceInfoProvider;
import com.softweb.comsquare_android.domain.providers.RadioInfoProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationParser;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.PlayServicesLocationProvider;
import com.softweb.comsquare_android.domain.providers.network.IpApiProvider;
import com.softweb.comsquare_android.domain.providers.network.IpProvider;
import com.softweb.comsquare_android.domain.providers.network.NetworkInfoProvider;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.device_screen.view.DeviceInfoPresenter;
import com.softweb.comsquare_android.home_screen.device_screen.view.DeviceInfoView;
import com.softweb.comsquare_android.home_screen.device_screen.view.ExportInfoCache;
import com.softweb.comsquare_android.home_screen.history_screen.HistoryPresenter;
import com.softweb.comsquare_android.home_screen.history_screen.HistoryView;
import com.softweb.comsquare_android.home_screen.interactor.BeginTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoRefreshClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DisplayUnitSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DownloadTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizeSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizesClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.QuestionaireItemClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SettingsLoadInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SpeedTestingValueSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.TestStateInteractor;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsPresenter;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsView;
import com.softweb.comsquare_android.home_screen.test_screen.QuestionaireView;
import com.softweb.comsquare_android.home_screen.test_screen.TestsPresenter;
import com.softweb.comsquare_android.home_screen.test_screen.TestsView;
import com.softweb.comsquare_android.utils.App;
import com.softweb.comsquare_android.utils.AppExecutors;
import com.softweb.comsquare_android.utils.DependencyUtil;
import com.softweb.comsquare_android.utils.PrefHelper;

import java.util.ArrayList;
import java.util.concurrent.Executor;

import butterknife.BindView;

public class HomeActivity extends BaseActivity {

  @BindView(R.id.toolbar)
  protected Toolbar toolbar;

  @BindView(R.id.bnv_home)
  protected BottomNavigationView bottomNavigationView;

  private final int FRAGMENT_CONTAINER = R.id.fl_home_view_container;

  private static final int REQ_CODE_ALL_PERMISSIONS = 34;

  // Fragment subviews

  private DeviceInfoView deviceInfoView;
  private QuestionaireView questionaireView;
  private HistoryView historyView;
  private TestsView testsView;
  private SettingsView settingsView;

  @Override
  protected void onStart() {
    super.onStart();

    // Check needed permissions and do not proceed with anything if they are not granted

    // TODO Move this to a separate interactor

    if (!App.hasPermissions(this, Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

      // Request it
      ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
              Manifest.permission.READ_PHONE_STATE,
              Manifest.permission.ACCESS_WIFI_STATE,
              Manifest.permission.WRITE_EXTERNAL_STORAGE},
          REQ_CODE_ALL_PERMISSIONS);
    }

  }

  private void showTestsScreen() {

    testsView.injectPresenter(
        DependencyUtil.getInstance().getTestsPresenter());

    SFM().beginTransaction()
        .replace(FRAGMENT_CONTAINER, testsView)
        .commit();
  }

  private void showHistoryScreen() {

    historyView.injectPresenter(
        DependencyUtil.getInstance().getHistoryPresenter());

    SFM().beginTransaction()
        .replace(FRAGMENT_CONTAINER, historyView)
        .commit();
  }

  private void showDeviceInfoScreen() {

    deviceInfoView.injectDevicePresenter(
        DependencyUtil.getInstance().getDeviceInfoPresenter());

    SFM().beginTransaction()
        .replace(FRAGMENT_CONTAINER, deviceInfoView)
        .commit();
  }

  private void showSettingsScreen() {

    settingsView.injectPresenter(
        DependencyUtil.getInstance().getSettingsPresenter());

    SFM().beginTransaction()
        .replace(FRAGMENT_CONTAINER, settingsView)
        .commit();
  }

  private void initSubViews(){

    questionaireView = QuestionaireView.newInstance(TestEnvironmentProvider.getInstance().getTestEnvironments());
    testsView = TestsView.newInstance();
    deviceInfoView = DeviceInfoView.newInstance();
    historyView = HistoryView.newInstance();
    settingsView = SettingsView.newInstance();

  }

  @Override
  protected void initViews() {

    super.initViews();

    initSubViews(); // Init fragment views

    // Init all dependencies here

    DependencyUtil.getInstance().initAll(this,
        deviceInfoView,
        questionaireView,
        testsView,
        settingsView);

    // Configure the navigation menu

    bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

      switch (item.getItemId()) {
        case R.id.menu_home_test:
          showTestsScreen();
          return true;
        case R.id.menu_home_history:
          showHistoryScreen();
          return true;
        case R.id.menu_home_settings:
          showSettingsScreen();
          return true;
        case R.id.menu_home_device:
          showDeviceInfoScreen();
          return true;
      }
      return true;
    });

    showTestsScreen(); // Tests screen opens by default
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (permissions.length == 3)
      dispatchPermissionsGiven(true);
    else
      dispatchPermissionsGiven(false);
  }

  private void dispatchPermissionsGiven(boolean allGiven) {

  }

  @Override
  protected int getLayoutId() {
    return R.layout.activity_home;
  }

  @Nullable
  @Override
  protected Toolbar getToolbar() {
    return toolbar;
  }

  @Override
  protected String getToolbarTitle() {
    return "CommSquare";
  }

  @Override
  protected boolean homeAsUpEnabled() {
    return false;
  }


}
