package com.softweb.comsquare_android.home_screen.interactor;

import android.os.Environment;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.ExportJSONHelper;
import com.softweb.comsquare_android.home_screen.Contract;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executor;

public class ExportJSONInteractor extends AbstractInteractor {

  private final ExportJSONHelper exportJSONHelper;
  private final Contract.DeviceInfoView deviceInfoView;
  private final File outputFileDirectory;

  public ExportJSONInteractor(Executor mainThread,
                              Executor executionThread,
                              ExportJSONHelper exportJSONHelper,
                              Contract.DeviceInfoView deviceInfoView,
                              File fileOutputDirectory) {

    super(mainThread, executionThread);
    this.exportJSONHelper = exportJSONHelper;
    this.deviceInfoView = deviceInfoView;
    this.outputFileDirectory = fileOutputDirectory;
  }

  private boolean checkIfExternalIsAvailable(){
    String state = Environment.getExternalStorageState();
    return state.equals(Environment.MEDIA_MOUNTED);
  }


  @Override
  public void run() {

    final String allDataJson = exportJSONHelper.exportAllInfo();

    final boolean isExternalAvailable = checkIfExternalIsAvailable();

    // Store the json file in external memory

    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY-hh-mm-ss");
    final Date date = Calendar.getInstance().getTime();

    final String dateStr = simpleDateFormat.format(date);

    final String filename = "commsquare_device_stats" + dateStr + ".json";

    File file = new File(outputFileDirectory, filename);
    FileOutputStream outputStream = null;

    try {

      outputStream = new FileOutputStream(file);
      outputStream.write(allDataJson.getBytes());

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (outputStream != null)
          outputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    // Show message displayed

    mainThread.execute(() -> deviceInfoView.showActionMessage("JSON exported successfully"));

  }
}
