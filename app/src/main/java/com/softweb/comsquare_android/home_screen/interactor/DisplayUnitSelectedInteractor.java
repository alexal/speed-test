package com.softweb.comsquare_android.home_screen.interactor;

import android.content.SharedPreferences;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.utils.PrefHelper;

import java.util.concurrent.Executor;

public class DisplayUnitSelectedInteractor extends AbstractInteractor {

  private final Contract.SettingsView settingsView;
  private final SharedPreferences.Editor editor;
  private int selectedDisplayUnit;

  public DisplayUnitSelectedInteractor(Executor mainThread,
                                       Executor executionThread,
                                       Contract.SettingsView settingsView,
                                       SharedPreferences.Editor editor) {
    super(mainThread, executionThread);
    this.settingsView = settingsView;
    this.editor = editor;
  }

  public void setSelectedDisplayUnit(int selectedDisplayUnit) {
    this.selectedDisplayUnit = selectedDisplayUnit;
  }

  @Override
  public void run() {

    editor.putInt(PrefHelper.SPEED_DISPLAY_UNIT, selectedDisplayUnit).commit();

    mainThread.execute(() -> settingsView.setSpeedDisplayUnit(selectedDisplayUnit));
  }
}
