package com.softweb.comsquare_android.home_screen.interactor;

import com.softweb.comsquare_android.base.AbstractInteractor;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.Contract;
import com.softweb.comsquare_android.home_screen.settings_screen.FileSizesView;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsPresenter;
import com.softweb.comsquare_android.utils.view.ListDialogFragment;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class FileSizesClickInteractor extends AbstractInteractor {

  private final Contract.SettingsView settingsView;

  public FileSizesClickInteractor(Executor mainThread,
                                  Executor executionThread,
                                  Contract.SettingsView settingsView) {
    super(mainThread, executionThread);
    this.settingsView = settingsView;
  }

  @Override
  public void run() {

    // Fetch all the available sizes

    ListDialogFragment.ItemView file1 = new ListDialogFragment.ItemView(TestFilesRepository.MB_1);
    ListDialogFragment.ItemView file3 = new ListDialogFragment.ItemView(TestFilesRepository.MB_3);
    ListDialogFragment.ItemView file5 = new ListDialogFragment.ItemView(TestFilesRepository.MB_5);
    ListDialogFragment.ItemView file10 = new ListDialogFragment.ItemView(TestFilesRepository.MB_10);
    ListDialogFragment.ItemView file12 = new ListDialogFragment.ItemView(TestFilesRepository.MB_12);
    ListDialogFragment.ItemView file50 = new ListDialogFragment.ItemView(TestFilesRepository.MB_50);
    ListDialogFragment.ItemView file100 = new ListDialogFragment.ItemView(TestFilesRepository.MB_100);
    ListDialogFragment.ItemView file180 = new ListDialogFragment.ItemView(TestFilesRepository.MB_180);
    ListDialogFragment.ItemView file200 = new ListDialogFragment.ItemView(TestFilesRepository.MB_200);
    ListDialogFragment.ItemView file1500 = new ListDialogFragment.ItemView(TestFilesRepository.MB_1500);

    ArrayList<ListDialogFragment.ItemView> data = new ArrayList<>();

    data.add(file1);data.add(file3);data.add(file5);data.add(file10);data.add(file12);
    data.add(file50);data.add(file100);data.add(file180);data.add(file200);data.add(file1500);

    final FileSizesView fileSizesView = FileSizesView.newInstance(data);

    mainThread.execute(() -> settingsView.showFileSizesView(fileSizesView));
  }
}
