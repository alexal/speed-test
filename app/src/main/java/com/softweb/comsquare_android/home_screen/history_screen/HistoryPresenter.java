package com.softweb.comsquare_android.home_screen.history_screen;

import com.softweb.comsquare_android.base.AbstractPresenter;

public class HistoryPresenter implements AbstractPresenter{

    private Contract.View mView;

    @Override
    public void onAttach() {

        // Query the history and show it

        mView.showHistoryResults();
    }

    @Override
    public void onDetach() {

    }

    public void attachView(Contract.View pView){
        mView = pView;
    }
}
