package com.softweb.comsquare_android.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Class managing the threads mechanism in the app.
 * Use this class to execute tasks in their corresponding executor.
 */
public class AppExecutors {

    private static AppExecutors INSTANCE;

    private final Executor mMainThread; // Used for UI
    private final Executor diskIO; // Used for database transactions
    private final Executor networkThread; // Used for network calls

    public static AppExecutors getInstance(){
        if (INSTANCE == null){
            INSTANCE = new AppExecutors();
        }
        return INSTANCE;
    }

    private AppExecutors(Executor pDiskIO, Executor pNetworkIO, Executor pMainThread) {
        this.mMainThread   = pMainThread;
        this.diskIO        = pDiskIO;
        this.networkThread = pNetworkIO;
    }

    private AppExecutors(){
        this(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3),
                new MainThreadExecutor());
    }

    public Executor mainThread() {
        return mMainThread;
    }

    public Executor diskIO() {
        return diskIO;
    }

    public Executor networkThread() {
        return networkThread;
    }

    private static class MainThreadExecutor implements Executor{

        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable runnable) {
            mainThreadHandler.post(runnable);
        }
    }
}
