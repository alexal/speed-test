package com.softweb.comsquare_android.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public final class App {

  // ------------------- Constants --------------------------------

  private static final String CUSTOMER_ID_KEY = "customer_id";
  public static final long TEST_TIME_DURATION_MILLIS = 20000L; // 20 secs

  // -------------------------------------------------------------

  private App() {}

  public static boolean hasPermissions(Context context, String... permissions) {
    if (context != null && permissions != null) {
      for (String permission : permissions) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * TODO Replace with UUID
   *
   * @return
   */
  private static String generateCustomerId() {
    return "CUSTOMER_ID";
  }

  /**
   * Get the customer id of the application. This ID is unique for each customer-user of the app.
   * It is generated at the first call of this function and then stored locally in device.
   *
   * @return
   */
  public static String getCustomerID(Context context) {

    String storedCustomerID = PrefHelper.prefs(context).getString(CUSTOMER_ID_KEY, "");

    if (!storedCustomerID.isEmpty()) {
      return storedCustomerID;
    } else {
      //Generate it and store it
      String generatedID = generateCustomerId();

      PrefHelper.edit(context).putString(CUSTOMER_ID_KEY, generatedID).apply();

      return generatedID;
    }
  }



}
