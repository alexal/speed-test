package com.softweb.comsquare_android.utils.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softweb.comsquare_android.R;

import java.util.ArrayList;
import java.util.List;

public class ListDialogAdapter extends RecyclerView.Adapter<ListDialogAdapter.ItemViewHolder>{

  private final List<ListDialogFragment.ItemView> data;
  private final ListDialogFragment.ItemClickListener clickListener;

  ListDialogAdapter(List<ListDialogFragment.ItemView> data,
                    ListDialogFragment.ItemClickListener clickListener){
    this.data = new ArrayList<>(data);
    this.clickListener = clickListener;
  }

  @NonNull
  @Override
  public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new ItemViewHolder(LayoutInflater.from(parent.getContext())
    .inflate(R.layout.item_view_list_dialog, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
    holder.bind(data.get(position),clickListener);
  }

  @Override
  public int getItemCount() {
    return data.size();
  }

  static class ItemViewHolder extends RecyclerView.ViewHolder{

    TextView itemValueView;

    ItemViewHolder(View itemView) {
      super(itemView);
      itemValueView = itemView.findViewById(R.id.tv_list_dialog_item);
    }

    void bind(final ListDialogFragment.ItemView item, final ListDialogFragment.ItemClickListener clickListener){
      itemValueView.setText(item.itemValue);
      itemValueView.setOnClickListener(view -> clickListener.onItemClick(item));
    }
  }
}
