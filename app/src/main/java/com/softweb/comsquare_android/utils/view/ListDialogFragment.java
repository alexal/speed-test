package com.softweb.comsquare_android.utils.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.softweb.comsquare_android.R;
import com.softweb.comsquare_android.base.BaseDialogFragmentView;

import java.util.List;

import butterknife.BindView;

/**
 * Util dialog fragment which displays a list of single textview.
 */
public abstract class ListDialogFragment extends BaseDialogFragmentView {

  /**
   * Base render class. The list is super simple, it just diplays a string text.
   */
  public static class ItemView implements Parcelable {
    final String itemValue;

    public ItemView(String itemValue) {
      this.itemValue = itemValue;
    }

    public String getItemValue() {
      return itemValue;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeString(this.itemValue);
    }

    protected ItemView(Parcel in) {
      this.itemValue = in.readString();
    }

    public static final Parcelable.Creator<ItemView> CREATOR = new Parcelable.Creator<ItemView>() {
      @Override
      public ItemView createFromParcel(Parcel source) {
        return new ItemView(source);
      }

      @Override
      public ItemView[] newArray(int size) {
        return new ItemView[size];
      }
    };
  }

  // ---------------------------- ---------------------------- ----------------------------

  public interface ItemClickListener{
    void onItemClick(ItemView item);
  }

  @BindView(R.id.tv_list_dialog_title)
  protected TextView tvListDialogTitle;

  @BindView(R.id.rv_list_dialog_items)
  protected RecyclerView rvListDialogItems;

  @Override
  protected int getLayoutId() {
    return R.layout.frag_view_list_dialog;
  }

  @Override
  protected void initViews(View root) {
    tvListDialogTitle.setText(getDialogTitle());

    LinearLayoutManager llm = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
    rvListDialogItems.setLayoutManager(llm);
    final ListDialogAdapter adapter = new ListDialogAdapter(getItems(), getClickListener());
    rvListDialogItems.setAdapter(adapter);
  }

  protected abstract List<ItemView> getItems();
  protected abstract String getDialogTitle();
  protected abstract ItemClickListener getClickListener();
}
