package com.softweb.comsquare_android.utils;

import org.greenrobot.eventbus.EventBus;

/**
 * Util class to post custom events on bus.
 *
 * @author alexal
 */
public final class Bus {

  private static final EventBus BUS = EventBus.getDefault();

  private Bus() {
  }

  /**
   * Subscribes the provider object to the event bus. When subscribed, this object
   * can listen to any events posted through the bus.
   * Check more at https://github.com/greenrobot/EventBus
   *
   * @param subscriber
   */
  public static void subscribe(Object subscriber) {
    BUS.register(subscriber);
  }

  public static void unregister(Object subscriber) {
    BUS.unregister(subscriber);
  }

  public static void post(Event busEvent) {
    BUS.post(busEvent);
  }

  public static void post(EventWithArgs busEvent) {
    BUS.post(busEvent);
  }

  public static class Event {
  }

  public static class EventWithArgs<A> {
    private A argument;

    public EventWithArgs(A argument) {
      this.argument = argument;
    }

    public A getArgument() {
      return argument;
    }
  }

}
