package com.softweb.comsquare_android.utils;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class AnimationUtil {

  private AnimationUtil(){}

  public static void animateBlink(ImageView imageView){
    Animation animation = new AlphaAnimation(1, 0);
    animation.setDuration(500);
    animation.setInterpolator(new LinearInterpolator());
    animation.setRepeatCount(Animation.INFINITE);
    animation.setRepeatMode(Animation.REVERSE);
    imageView.startAnimation(animation);
  }

  public static void stopBlinkAnimation(ImageView imageView){
    imageView.clearAnimation();
  }
}
