package com.softweb.comsquare_android.utils;

import android.content.Context;
import android.content.SharedPreferences;

public final class PrefHelper {

  private static final String PREF_FILE = "PREF_FILE";

  // -------------------------- App preferences keys -------------------------

  public static final String TEST_FILE_SIZE = "test_file_size";
  public static final String SPEED_DISPLAY_UNIT = "speed_display_unit";
  public static final String SPEED_TESTING = "speed_testing";
  // -------------------------------------------------------------------------

  private PrefHelper() {
  }

  public static SharedPreferences prefs(Context context) {
    return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
  }

  public static SharedPreferences.Editor edit(Context context) {
    return prefs(context).edit();
  }
}
