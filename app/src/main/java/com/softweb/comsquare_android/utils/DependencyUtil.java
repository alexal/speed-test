package com.softweb.comsquare_android.utils;

import android.content.Context;
import android.location.Geocoder;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.google.android.gms.location.LocationServices;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayer;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayerKb;
import com.softweb.comsquare_android.domain.http_speeds.SpeedDisplayerMb;
import com.softweb.comsquare_android.domain.providers.DeviceInfoProvider;
import com.softweb.comsquare_android.domain.providers.RadioInfoProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationParser;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.LocationProvider;
import com.softweb.comsquare_android.domain.providers.location.LocationProvider.PlayServicesLocationProvider;
import com.softweb.comsquare_android.domain.providers.network.IpApiProvider;
import com.softweb.comsquare_android.domain.providers.network.IpProvider;
import com.softweb.comsquare_android.domain.providers.network.NetworkInfoProvider;
import com.softweb.comsquare_android.domain.remote.TestFilesRepository;
import com.softweb.comsquare_android.home_screen.device_screen.view.DeviceInfoPresenter;
import com.softweb.comsquare_android.home_screen.device_screen.view.DeviceInfoView;
import com.softweb.comsquare_android.home_screen.device_screen.view.ExportInfoCache;
import com.softweb.comsquare_android.home_screen.history_screen.HistoryPresenter;
import com.softweb.comsquare_android.home_screen.interactor.BeginTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DeviceInfoRefreshClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DisplayUnitSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.DownloadTestInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizeSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.FileSizesClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.QuestionaireItemClickInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SettingsLoadInteractor;
import com.softweb.comsquare_android.home_screen.interactor.SpeedTestingValueSelectedInteractor;
import com.softweb.comsquare_android.home_screen.interactor.TestStateInteractor;
import com.softweb.comsquare_android.home_screen.interactor.UploadTestInteractor;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsPresenter;
import com.softweb.comsquare_android.home_screen.settings_screen.SettingsView;
import com.softweb.comsquare_android.home_screen.test_screen.QuestionaireView;
import com.softweb.comsquare_android.home_screen.test_screen.TestsPresenter;
import com.softweb.comsquare_android.home_screen.test_screen.TestsView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.concurrent.Executor;

import timber.log.Timber;

import static android.content.Context.TELEPHONY_SUBSCRIPTION_SERVICE;

public final class DependencyUtil {

  //----------------------- Constants -------------------------------

  private static final String TEST_FILE_FILENAME = "dummy.txt";

  private DependencyUtil(){}
  private static final DependencyUtil INSTANCE = new DependencyUtil();

  // ---------------------- Cached dependencies ----------------------

  private DeviceInfoClickInteractor mDeviceInfoClickInteractor;
  private DeviceInfoRefreshClickInteractor mDeviceInfoRefreshClickInteractor;
  private BeginTestInteractor mBeginTestInteractor;
  private QuestionaireItemClickInteractor mQuestionaireItemClickInteractor;
  private DownloadTestInteractor mDownloadTestInteractor;
  private UploadTestInteractor mUploadTestInteractor;
  private FileSizesClickInteractor mFileSizesClickInteractor;
  private FileSizeSelectedInteractor mFileSizeSelectedInteractor;
  private SettingsLoadInteractor mSettingsLoadInteractor;
  private DisplayUnitSelectedInteractor mDisplayUnitSelectedInteractor;
  private SpeedTestingValueSelectedInteractor mSpeedTestingValueSelectedInteractor;
  private TestStateInteractor mTestStateInteractor;

  private TestsPresenter mTestsPresenter;
  private DeviceInfoPresenter mDeviceInfoPresenter;
  private HistoryPresenter mHistoryPresenter;
  private SettingsPresenter mSettingsPresenter;

  // -----------------------------------------------------------------

  public static DependencyUtil getInstance(){
    return INSTANCE;
  }

  public void initAll(Context context,
                      DeviceInfoView deviceInfoView,
                      QuestionaireView questionaireView,
                      TestsView testsView,
                      SettingsView settingsView){

    // Interactors
    mDeviceInfoClickInteractor = initDeviceInfoClickInteractor(context, deviceInfoView);
    mDeviceInfoRefreshClickInteractor = initDeviceInfoRefreshClickInteractor(context, deviceInfoView);
    mDeviceInfoPresenter = initDeviceInfoPresenter(context, deviceInfoView);
    mBeginTestInteractor = initBeginTestInteractor(questionaireView, testsView);
    mQuestionaireItemClickInteractor = initQuestionaireItemClickInteractor(questionaireView);
    mUploadTestInteractor = initUploadTestInteractor(context, testsView);
    mDownloadTestInteractor = initDownloadTestInteractor(context, testsView);
    mFileSizesClickInteractor = initFileSizesClickInteractor(settingsView);
    mFileSizeSelectedInteractor = initFileSizeSelectedInteractor(context, settingsView);
    mSettingsLoadInteractor = initSettingsLoadInteractor(context, settingsView);
    mDisplayUnitSelectedInteractor = initDisplayUnitSelectedInteractor(settingsView, context);
    mTestStateInteractor = TestStateInteractor.getInstance();
    mSpeedTestingValueSelectedInteractor = initSpeedTestingValueSelectedInteractor(settingsView, context);

    // Presenters
    mTestsPresenter = initTestsPresenter(
        mBeginTestInteractor,
        mQuestionaireItemClickInteractor,
        mTestStateInteractor,
        mDownloadTestInteractor);
    mHistoryPresenter = initHistoryPresenter();
    mSettingsPresenter = initSettingsPresenter(
        mFileSizesClickInteractor,
        mFileSizeSelectedInteractor,
        mSettingsLoadInteractor, mDisplayUnitSelectedInteractor,
        mSpeedTestingValueSelectedInteractor);

  }

  private ExportInfoCache getExportInfoCache(){
    return ExportInfoCache.getINSTANCE();
  }

  private DeviceInfoClickInteractor initDeviceInfoClickInteractor(@NonNull Context context,
                                                                  @NonNull DeviceInfoView deviceInfoView){

    final Executor mainThead = AppExecutors.getInstance().mainThread();
    final Executor executionThread = AppExecutors.getInstance().networkThread();
    final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    final WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    final RadioInfoProvider radioInfoProvider = new RadioInfoProvider(telephonyManager, wifiManager);
    final DeviceInfoProvider deviceInfoProvider = new DeviceInfoProvider(telephonyManager, context.getPackageManager(), context);
    final IpProvider ipProvider = new IpApiProvider();
    final SubscriptionManager subscriptionManager = (SubscriptionManager) context.getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE);
    final LocationProvider locationProvider = new PlayServicesLocationProvider(
        LocationServices.getFusedLocationProviderClient(context),
        new LocationParser(
            new Geocoder(context)));

    final NetworkInfoProvider.Builder builder = new NetworkInfoProvider.Builder();
    builder.setWifiManager(wifiManager)
        .setIpProvider(ipProvider)
        .setpTelephonyManager(telephonyManager)
        .setSubscriptionManager(subscriptionManager);

    final NetworkInfoProvider networkInfoProvider = builder.createNetworkInfoProvider();
    final ExportInfoCache exportInfoCache = getExportInfoCache();

    return new DeviceInfoClickInteractor(
        mainThead,
        executionThread,
        deviceInfoView,
        radioInfoProvider,
        deviceInfoProvider,
        networkInfoProvider,
        locationProvider,
        exportInfoCache);
  }

  private DeviceInfoRefreshClickInteractor initDeviceInfoRefreshClickInteractor(@NonNull Context context,
                                                                                DeviceInfoView deviceInfoView){
    return new DeviceInfoRefreshClickInteractor(AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        deviceInfoView,
        initDeviceInfoClickInteractor(context, deviceInfoView));
  }

  private DeviceInfoPresenter initDeviceInfoPresenter(@NonNull Context context,
                                                      DeviceInfoView deviceInfoView){
    return new DeviceInfoPresenter(
        initDeviceInfoClickInteractor(context, deviceInfoView),
        initDeviceInfoRefreshClickInteractor(context, deviceInfoView),
        getExportInfoCache());
  }

  private BeginTestInteractor initBeginTestInteractor(QuestionaireView questionaireView,
                                                      TestsView testsView){
    return new BeginTestInteractor(AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        questionaireView,
        testsView);
  }

  private QuestionaireItemClickInteractor initQuestionaireItemClickInteractor(QuestionaireView questionaireView){
    return new QuestionaireItemClickInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        questionaireView);
  }

  private DownloadTestInteractor initDownloadTestInteractor(@NonNull Context context,
                                                            TestsView testsView){
    final SpeedDisplayer speedDisplayer;
    final int speedDisplaySelected = PrefHelper.prefs(context)
        .getInt(PrefHelper.SPEED_DISPLAY_UNIT, SettingsView.SPEED_DISPLAY_UNIT_KB);

    if (speedDisplaySelected == SettingsView.SPEED_DISPLAY_UNIT_KB)
      speedDisplayer = new SpeedDisplayerKb();
    else
      speedDisplayer = new SpeedDisplayerMb();

    final String downloadFileSizeID = PrefHelper.prefs(context)
        .getString(PrefHelper.TEST_FILE_SIZE,
        TestFilesRepository.MB_50); // Default

    final int speedTestMethod = PrefHelper.prefs(context).getInt(
        PrefHelper.SPEED_TESTING, SettingsView.SPEED_TESTING_TIME_BASED);

    final boolean timeBasedTesting = speedTestMethod == SettingsView.SPEED_TESTING_TIME_BASED;

    FileOutputStream internalOutputStream = null;
    File outputFile = new File(context.getFilesDir(), TEST_FILE_FILENAME);

    try {
      internalOutputStream = context.openFileOutput(TEST_FILE_FILENAME, Context.MODE_PRIVATE);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    return new DownloadTestInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().networkThread(),
        TestFilesRepository.getINSTANCE(),
        testsView,
        speedDisplayer,
        downloadFileSizeID,
        timeBasedTesting,
        internalOutputStream,
        outputFile,
        mUploadTestInteractor);
  }

  private UploadTestInteractor initUploadTestInteractor(Context context,
                                                        TestsView testsView){

    final SpeedDisplayer speedDisplayer;
    final int speedDisplaySelected = PrefHelper.prefs(context)
        .getInt(PrefHelper.SPEED_DISPLAY_UNIT, SettingsView.SPEED_DISPLAY_UNIT_KB);

    if (speedDisplaySelected == SettingsView.SPEED_DISPLAY_UNIT_KB)
      speedDisplayer = new SpeedDisplayerKb();
    else
      speedDisplayer = new SpeedDisplayerMb();

    final File inputFile = new File(context.getFilesDir(), TEST_FILE_FILENAME);
    FileInputStream fileInputStream = null;

    try {
      fileInputStream = new FileInputStream(inputFile);
    } catch (FileNotFoundException e) {
      Timber.e(e);
    }

    return new UploadTestInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        testsView,
        TestFilesRepository.getINSTANCE(),
        speedDisplayer,
        fileInputStream,
        inputFile);
  }


  private FileSizesClickInteractor initFileSizesClickInteractor(SettingsView settingsView){
    return new FileSizesClickInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        settingsView);
  }

  private FileSizeSelectedInteractor initFileSizeSelectedInteractor(Context context,
                                                                    SettingsView settingsView){
    return new FileSizeSelectedInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        PrefHelper.edit(context),
        settingsView);
  }

  private SettingsLoadInteractor initSettingsLoadInteractor(Context context,
                                                            SettingsView settingsView){
    return new SettingsLoadInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        PrefHelper.prefs(context),
        settingsView);
  }

  private DisplayUnitSelectedInteractor initDisplayUnitSelectedInteractor(SettingsView settingsView,
                                                                          Context context){
    return new DisplayUnitSelectedInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        settingsView,
        PrefHelper.edit(context));
  }

  private SpeedTestingValueSelectedInteractor initSpeedTestingValueSelectedInteractor(SettingsView settingsView,
                                                                                      Context context){
    return new SpeedTestingValueSelectedInteractor(
        AppExecutors.getInstance().mainThread(),
        AppExecutors.getInstance().diskIO(),
        settingsView,
        PrefHelper.edit(context));
  }

  private TestsPresenter initTestsPresenter(
      BeginTestInteractor beginTestInteractor,
      QuestionaireItemClickInteractor questionaireItemClickInteractor,
      TestStateInteractor testStateInteractor,
      DownloadTestInteractor downloadTestInteractor){

    return new TestsPresenter(
        beginTestInteractor,
        questionaireItemClickInteractor,
        testStateInteractor,
        downloadTestInteractor);
  }

  private HistoryPresenter initHistoryPresenter(){
    return new HistoryPresenter();
  }

  private SettingsPresenter initSettingsPresenter(

      FileSizesClickInteractor fileSizesClickInteractor,
      FileSizeSelectedInteractor fileSizeSelectedInteractor,
      SettingsLoadInteractor settingsLoadInteractor,
      DisplayUnitSelectedInteractor displayUnitSelectedInteractor,
      SpeedTestingValueSelectedInteractor speedTestingValueSelectedInteractor){

    return new SettingsPresenter(fileSizesClickInteractor,
        fileSizeSelectedInteractor,
        settingsLoadInteractor,
        displayUnitSelectedInteractor,
        speedTestingValueSelectedInteractor);
  }

  public TestsPresenter getTestsPresenter() {
    return mTestsPresenter;
  }

  public HistoryPresenter getHistoryPresenter() {
    return mHistoryPresenter;
  }

  public SettingsPresenter getSettingsPresenter() {
    return mSettingsPresenter;
  }

  public DeviceInfoPresenter getDeviceInfoPresenter() {
    return mDeviceInfoPresenter;
  }
}
