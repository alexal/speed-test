package com.softweb.comsquare_android.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import butterknife.ButterKnife;

/**
 * Base activity class. Contains many utility methods to setup your activity a bit faster.
 */
public abstract class BaseActivity extends AppCompatActivity{

    public static Handler mainHandler = new Handler(Looper.getMainLooper());

    //Base TestView stuff and MVP boilerplate

    @Override
    protected void onCreate(@Nullable final Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);

        this.setContentView(this.getLayoutId());

        ButterKnife.bind(this);

        //Set toolbar
        final Toolbar toolbar = this.getToolbar();

        if (toolbar != null) {
            this.setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        final ActionBar ab = this.getSupportActionBar();
        if (ab != null) {

            ab.setDisplayHomeAsUpEnabled(this.homeAsUpEnabled());
        }

        this.setTitle(this.getToolbarTitle());

        this.initViews();
    }

    /**
     * Reference to the layout.
     *
     * @return
     */
    @LayoutRes
    protected abstract int getLayoutId();

    /**
     * Reference to the toolbar.
     *
     * @return
     */
    @Nullable
    protected abstract Toolbar getToolbar();

    /**
     * The title in the toolbar.
     *
     * @return
     */
    protected abstract String getToolbarTitle();


    /**
     * Add the back button.
     *
     * @return
     */
    protected abstract boolean homeAsUpEnabled();

    @IdRes
    protected int getFragmentContainer(){
        return 0;
    }

    //Helper methods ---------------------------------------------------------------------------->

    protected String getAppString(@StringRes int stringId){
        return getString(stringId);
    }

    /**
     * Hides the system status bar.
     */
    protected void hideStatusBar(){
        View decorView = getWindow().getDecorView();
        int uiOption = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOption);
    }

    public void showMessage(String message){
        Toast.makeText(this,
                message,
                Toast.LENGTH_SHORT)
                .show();
    }

    protected FragmentTransaction transaction(){
        return getSupportFragmentManager().beginTransaction();
    }

    /**
     * Runs the given runnable which should contain a fragment transaction in the main handler
     * with a small delay of 500L ms.
     * @param tranactionRunnable
     */
    protected void showDelayed(Runnable tranactionRunnable){

        mainHandler.postDelayed(tranactionRunnable,
                500L);

    }

    protected void showDelayed(Runnable transactionRunnable,
                               long delay){

        mainHandler.postDelayed(transactionRunnable,
                delay);
    }

    /**
     * Called after the view initialization. This method is a good place to make any needed
     * view configurations.
     */
    protected void initViews(){}

    /**
     * Returns the root view of the controller.
     * @return
     */
    protected View getRootView(){
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }

    protected FragmentManager SFM(){
        return getSupportFragmentManager();
    }

    /**
     * Sets the given elevation to the provided views.
     * @param elevation
     * @param views
     */
    public static void setElevations(int elevation,View... views){

        for (int i = 0; i < views.length; i++) {
            ViewCompat.setElevation(views[i],elevation);
//			ViewCompat.setTranslationZ(views[i],elevation);
        }

    }


}
