package com.softweb.comsquare_android.base;

public interface Interactor extends Runnable{
  void execute();
}
