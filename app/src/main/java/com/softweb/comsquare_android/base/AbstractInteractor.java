package com.softweb.comsquare_android.base;


import java.util.concurrent.Executor;

public abstract class AbstractInteractor implements Interactor{

  protected final Executor mainThread;
  protected final Executor executionThread;

  private volatile boolean mIsRunning;
  private volatile boolean misCanceled;

  protected AbstractInteractor(Executor mainThread, Executor executionThread) {
    this.mainThread = mainThread;
    this.executionThread = executionThread;
  }

  public abstract void run();

  public void cancel(){
    mIsRunning = false;
    misCanceled = true;
  }

  public void onFinished(){
    mIsRunning = false;
    misCanceled = false;
  }

  @Override
  public void execute() {
    mIsRunning = true;
    executionThread.execute(this);
  }
}
