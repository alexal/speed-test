package com.softweb.comsquare_android.base;

/**
 * Wrapper interface of presenter layer in MVP design pattern.
 */
public interface AbstractPresenter {
    void onAttach();
    void onDetach();
}
