package com.softweb.comsquare_android.domain.http_speeds;

/**
 * Class which displa
 */
public class SpeedDisplayerKb implements SpeedDisplayer{

  @Override
  public String getDisplayedSpeed(long startMillis, long currentMillis, long currentSize) {

    final double currentSpeed;
    final double currentDownloadTime = currentMillis - startMillis;

    if (currentDownloadTime == 0) {
      currentSpeed = 0;
    } else {
      final double currentDownloadSecs = currentDownloadTime / 1000;
      final double donwloadedSizeKb = currentSize / 1024;
      currentSpeed = donwloadedSizeKb / currentDownloadSecs;
    }

    return String.format("%.0f kb/s",currentSpeed);
  }
}
