package com.softweb.comsquare_android.domain.phone_stats;

public final class RadioInfo {

  // Mutable values because they are configured dynamically

  private String startCellId;
  private String endCellId;
  private String startRnc;
  private String endRnc;

  private final int rssi;
  private final String wifiBssid;
  private final String wifiSsid;
  private final int lac;
  private final int tac;
  private final int pci;
  private final int psc;
  private final int bcch;
  private final int asu;
  private final int rscp;
  private final double level; // Signal strength level 0-4
  private final double timingAdvance;
  private final double ecio; // 0 >= value >=-24
  private final double sinr;
  private final String signal_string;
  private final String wifi_frequency_band_mhz;
  private final String wifi_frequency_mhz;
  private final int rsrp;
  private final int rsrq;
  private final int cqi;
  private final int rxLevel;

  private RadioInfo(String wifiBssid,
                    String wifiSsid,
                    int lac,
                    int tac,
                    int pci,
                    int psc,
                    int bcch,
                    int asu,
                    int rscp,
                    double level,
                    double timingAdvance,
                    double ecio,
                    double sinr,
                    int rssi,
                    String signalString,
                    String wifiFrequencyBandMhz,
                    String wifiFrequencyMhz,
                    int rsrp,
                    int rsrq,
                    int cqi,
                    int rxLevel) {

    this.wifiBssid = wifiBssid;
    this.wifiSsid = wifiSsid;
    this.lac = lac;
    this.tac = tac;
    this.pci = pci;
    this.psc = psc;
    this.bcch = bcch;
    this.asu = asu;
    this.rscp = rscp;
    this.level = level;
    this.timingAdvance = timingAdvance;
    this.ecio = ecio;
    this.sinr = sinr;
    this.rssi = rssi;
    this.signal_string = signalString;
    this.wifi_frequency_band_mhz = wifiFrequencyBandMhz;
    this.wifi_frequency_mhz = wifiFrequencyMhz;
    this.rsrp = rsrp;
    this.rsrq = rsrq;
    this.cqi = cqi;
    this.rxLevel = rxLevel;
  }

  public void setStartCellId(String startCellId) {
    this.startCellId = startCellId;
  }

  public void setEndCellId(String endCellId) {
    this.endCellId = endCellId;
  }

  public void setEndRnc(String endRnc) {
    this.endRnc = endRnc;
  }

  public void setStartRnc(String startRnc) {
    this.startRnc = startRnc;
  }

  public String getStartCellId() {
    return startCellId;
  }

  public String getEndCellId() {
    return endCellId;
  }

  public String getStartRnc() {
    return startRnc;
  }

  public String getEndRnc() {
    return endRnc;
  }

  public String getWifiBssid() {
    return wifiBssid;
  }

  public String getWifiSsid() {
    return wifiSsid;
  }

  public int getLac() {
    return lac;
  }

  public int getTac() {
    return tac;
  }

  public int getPci() {
    return pci;
  }

  public int getPsc() {
    return psc;
  }

  public int getBcch() {
    return bcch;
  }

  public int getAsu() {
    return asu;
  }

  public int getRscp() {
    return rscp;
  }

  public double getLevel() {
    return level;
  }

  public double getTimingAdvance() {
    return timingAdvance;
  }

  public double getEcio() {
    return ecio;
  }

  public double getSinr() {
    return sinr;
  }

  public int getRssi() {
    return rssi;
  }

  public String getSignal_string() {
    return signal_string;
  }

  public String getWifi_frequency_band_mhz() {
    return wifi_frequency_band_mhz;
  }

  public String getWifi_frequency_mhz() {
    return wifi_frequency_mhz;
  }

  public int getRsrp() {
    return rsrp;
  }

  public int getRsrq() {
    return rsrq;
  }

  public int getCqi() {
    return cqi;
  }

  public int getRxLevel() {
    return rxLevel;
  }

  @Override
  public String toString() {
    return "RadioInfo{" +
        "startCellId='" + startCellId + '\'' +
        ", endCellId='" + endCellId + '\'' +
        ", startRnc='" + startRnc + '\'' +
        ", endRnc='" + endRnc + '\'' +
        ", rssi=" + rssi +
        ", wifiBssid='" + wifiBssid + '\'' +
        ", wifiSsid='" + wifiSsid + '\'' +
        ", lac=" + lac +
        ", tac=" + tac +
        ", pci=" + pci +
        ", psc=" + psc +
        ", bcch=" + bcch +
        ", asu=" + asu +
        ", rscp=" + rscp +
        ", level=" + level +
        ", timingAdvance=" + timingAdvance +
        ", ecio=" + ecio +
        ", sinr=" + sinr +
        ", signal_string='" + signal_string + '\'' +
        ", wifi_frequency_band_mhz='" + wifi_frequency_band_mhz + '\'' +
        ", wifi_frequency_mhz='" + wifi_frequency_mhz + '\'' +
        ", rsrp=" + rsrp +
        ", rsrq=" + rsrq +
        ", cqi=" + cqi +
        ", rxLevel=" + rxLevel +
        '}';
  }

  /**
   * Builder class
   */
  public static class Builder {

    private String wifiBssid;
    private String wifiSsid;
    private int lac;
    private int tac;
    private int pci;
    private int psc;
    private int bcch;
    private int asu;
    private int rscp;
    private double level;
    private double timingAdvance;
    private double ecio;
    private double sinr;
    private int rssi;
    private String signalString;
    private String wifiFrequencyBandMhz;
    private String wifiFrequencyMhz;
    private int rsrp;
    private int rsrq;
    private int cqi;
    private int rxLevel;

    public Builder setSignalString(String signalString) {
      this.signalString = signalString;
      return this;
    }
    public Builder setWifiFrequencyBandMhz(String wifiFrequencyBandMhz) {
      this.wifiFrequencyBandMhz = wifiFrequencyBandMhz;
      return this;
    }

    public Builder setWifiFrequencyMhz(String wifiFrequencyMhz) {
      this.wifiFrequencyMhz = wifiFrequencyMhz;
      return this;
    }

    public Builder setRsrp(int rsrp) {
      this.rsrp = rsrp;
      return this;
    }

    public Builder setRsrq(int rsrq) {
      this.rsrq = rsrq;
      return this;
    }

    public Builder setCqi(int cqi) {
      this.cqi = cqi;
      return this;
    }

    public Builder setRxLevel(int rxLevel) {
      this.rxLevel = rxLevel;
      return this;
    }

    public Builder setWifiBssid(String wifiBssid) {
      this.wifiBssid = wifiBssid;
      return this;
    }

    public Builder setWifiSsid(String wifiSsid) {
      this.wifiSsid = wifiSsid;
      return this;
    }

    public Builder setLac(int lac) {
      this.lac = lac;
      return this;
    }

    public Builder setTac(int tac) {
      this.tac = tac;
      return this;
    }

    public Builder setPci(int pci) {
      this.pci = pci;
      return this;
    }

    public Builder setPsc(int psc) {
      this.psc = psc;
      return this;
    }

    public Builder setBcch(int bcch) {
      this.bcch = bcch;
      return this;
    }

    public Builder setAsu(int asu) {
      this.asu = asu;
      return this;
    }

    public Builder setRscp(int rscp) {
      this.rscp = rscp;
      return this;
    }

    public Builder setLevel(double level) {
      this.level = level;
      return this;
    }

    public Builder setTimingAdvance(double timingAdvance) {
      this.timingAdvance = timingAdvance;
      return this;
    }

    public Builder setEcio(double ecio) {
      this.ecio = ecio;
      return this;
    }

    public Builder setSinr(double sinr) {
      this.sinr = sinr;
      return this;
    }

    public Builder setRssi(int rssi) {
      this.rssi = rssi;
      return this;
    }

    public RadioInfo createRadioInfo() {
      return new RadioInfo(wifiBssid,
          wifiSsid,
          lac,
          tac,
          pci,
          psc,
          bcch,
          asu,
          rscp,
          level,
          timingAdvance,
          ecio,
          sinr,
          rssi,
          signalString,
          wifiFrequencyBandMhz,
          wifiFrequencyMhz,
          rsrp,
          rsrq,
          cqi,
          rxLevel);
    }
  }
}
