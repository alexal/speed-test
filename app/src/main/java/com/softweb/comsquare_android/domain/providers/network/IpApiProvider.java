package com.softweb.comsquare_android.domain.providers.network;

import android.support.annotation.Nullable;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Uses http://ip-api.com/json to get the IP address response as long with ISP name.
 */
public class IpApiProvider implements IpProvider {

  private static final String URL = "http://ip-api.com/json";

  /**
   * Response class used to easily parse the JSON.
   */
  private static class IpApiResponse {
    private String status;
    private String isp;
    private String query;

  }

  private IpApiResponse cachedResponse;

  @Nullable
  private IpApiResponse fetchResponseFromApi(){

    OkHttpClient okHttpClient = new OkHttpClient();
    Request request = new Request.Builder()
        .url(URL)
        .build();

    try {

      Response response = okHttpClient.newCall(request).execute();
      String responseStr = response.body().string();

      // Parse the response
      Moshi moshi = new Moshi.Builder().build();
      JsonAdapter<IpApiResponse> adapter = moshi.adapter(IpApiResponse.class);
      return adapter.fromJson(responseStr);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public String fetchProxyIPAddress() {

    if (cachedResponse != null)
      return cachedResponse.query;

    final IpApiResponse response = fetchResponseFromApi();

    if (response == null){ // Failed to fetch it because IO exception
      return "Unavailable IP";
    }

    // Repsonse is valid, cache it
    cachedResponse = response;

    return cachedResponse.query;
  }

  @Override
  public String fetchISPName() {

    if (cachedResponse != null)
      return cachedResponse.isp;

    final IpApiResponse response = fetchResponseFromApi();

    if (response == null)
      return "Unavailable ISP";

    // Repsonse is valid, cache it
    cachedResponse = response;

    return cachedResponse.isp;
  }


}
