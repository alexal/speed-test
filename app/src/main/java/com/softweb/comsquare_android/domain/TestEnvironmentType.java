package com.softweb.comsquare_android.domain;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TestEnvironmentType implements Parcelable {

  public static final String STATIC_INDOOR = "Indoors (home, office, cafeteria, etc.)";
  public static final String STATIC_OUTDOOR = "Outdoors sitting (park, bench, etc.)";
  public static final String MOVING_OUTDOOR = "Outdoors moving (car, train, etc.)";

  @StringDef({STATIC_INDOOR, STATIC_OUTDOOR, MOVING_OUTDOOR})
  @Retention(RetentionPolicy.SOURCE)
  public @interface TYPE {}

  @TYPE
  private final String environmentType;
  @DrawableRes
  private final int resourceID;

  public TestEnvironmentType(@TYPE String environmentType, @DrawableRes int resourceID) {
    this.environmentType = environmentType;
    this.resourceID = resourceID;
  }

  @DrawableRes
  public int getResourceID() {
    return resourceID;
  }

  @TYPE
  public String getEnvironmentType() {
    return environmentType;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.environmentType);
    dest.writeInt(this.resourceID);
  }

  protected TestEnvironmentType(Parcel in) {
    this.environmentType = in.readString();
    this.resourceID = in.readInt();
  }

  public static final Parcelable.Creator<TestEnvironmentType> CREATOR = new Parcelable.Creator<TestEnvironmentType>() {
    @Override
    public TestEnvironmentType createFromParcel(Parcel source) {
      return new TestEnvironmentType(source);
    }

    @Override
    public TestEnvironmentType[] newArray(int size) {
      return new TestEnvironmentType[size];
    }
  };
}
