package com.softweb.comsquare_android.domain.http_speeds;

public class SpeedDisplayerMb implements SpeedDisplayer{
  @Override
  public String getDisplayedSpeed(long startMillis, long currentMillis, long currentSize) {
    final double currentSpeed;
    final double currentDownloadTime = currentMillis - startMillis;

    if (currentDownloadTime == 0) {
      currentSpeed = 0;
    } else {
      final double currentDownloadSecs = currentDownloadTime / 1000;
      final double downloadedSizeKb = currentSize / 1024;
      final double downloadedSizeMb = downloadedSizeKb / 1024;
      currentSpeed = downloadedSizeMb / currentDownloadSecs;
    }

    return String.format("%.3f Mb/s",currentSpeed);
  }
}
