package com.softweb.comsquare_android.domain.phone_stats;

public class LocationInfo {

    private final double latitude;
    private final double longitude;
    private final double altitude;
    private final double speed;
    private final String country;
    private final String countryCode;
    private final String region;
    private final String city;
    private final String timezoneName;
    private final String timezoneOffset;
    private final double accuracy; // Accuracy of location data expressed in meters

    public LocationInfo(double latitude, double longitude, double altitude, double speed, String country, String countryCode, String region, String city, String timezoneName, String timezoneOffset, double accuracy) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.country = country;
        this.countryCode = countryCode;
        this.region = region;
        this.city = city;
        this.timezoneName = timezoneName;
        this.timezoneOffset = timezoneOffset;
        this.accuracy = accuracy;
    }

    public double getAltitude() {
        return altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }

    public String getTimezoneName() {
        return timezoneName;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    public double getAccuracy() {
        return accuracy;
    }

    /**
     * Builder class
     */
    public static class Builder {
        private double latitude;
        private double longitude;
        private double altitude;
        private double speed;
        private String country;
        private String countryCode;
        private String region;
        private String city;
        private String timezoneName;
        private String timezoneOffset;
        private double accuracy;

        public Builder setLatitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setAltitude(double altitude) {
            this.altitude = altitude;
            return this;
        }

        public Builder setSpeed(double speed) {
            this.speed = speed;
            return this;
        }

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder setCountryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder setRegion(String region) {
            this.region = region;
            return this;
        }

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setTimezoneName(String timezoneName) {
            this.timezoneName = timezoneName;
            return this;
        }

        public Builder setTimezoneOffset(String timezoneOffset) {
            this.timezoneOffset = timezoneOffset;
            return this;
        }

        public Builder setAccuracy(double accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public LocationInfo createLocationInfo() {
            return new LocationInfo(latitude, longitude, altitude, speed, country, countryCode, region, city, timezoneName, timezoneOffset, accuracy);
        }
    }
}
