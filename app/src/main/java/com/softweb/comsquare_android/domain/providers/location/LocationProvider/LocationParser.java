package com.softweb.comsquare_android.domain.providers.location.LocationProvider;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;
import com.softweb.comsquare_android.utils.AppExecutors;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import timber.log.Timber;

/**
 * Describes the parsing from the location object returned from Play services framework.
 */
public class LocationParser {

  private final Geocoder mGeocoder;

  /**
   * Constructor of the class
   *
   * @param pGeocoder the Geocoder to be used to parse the location.
   */
  public LocationParser(Geocoder pGeocoder) {
    mGeocoder = pGeocoder;
  }

  /**
   * Parses the location object to the domain location model.
   * The geocoding execution takes place on the network thread of the {@link AppExecutors}.
   *
   * @param parseCallback
   */
  public void parse(android.location.Location pLocation, @NonNull ParseCallback parseCallback) {

    if (pLocation == null) {
      parseCallback.onLocationParsed(null);
    } else {

      try {
        final List<Address> addresses = mGeocoder.getFromLocation(pLocation.getLatitude(), pLocation.getLongitude(), 3);

        // Parse to domain level object
        final LocationInfo parsedLocationInfo = parseToDomain(addresses.get(0), pLocation);

        // Deliver the result in the main thread

        parseCallback.onLocationParsed(parsedLocationInfo);

      } catch (IOException e) {
        e.printStackTrace();
        Timber.e(e);
        parseCallback.onLocationParsed(null);
      }
    }
  }

  /**
   * @param location
   * @return
   * @deprecated
   */
  private LocationInfo parseToDomain(android.location.Location location) {
    return new LocationInfo.Builder()
        .setLatitude(location.getLatitude())
        .setLongitude(location.getLongitude())
        .setAltitude(location.getAltitude())
        .setSpeed(location.getSpeed())
        .createLocationInfo();
  }

  private LocationInfo parseToDomain(Address address, android.location.Location location) {
    return new LocationInfo.Builder()
        .setLatitude(address.getLatitude())
        .setLongitude(address.getLongitude())
        .setAltitude(location.getAltitude())
        .setSpeed(location.getSpeed())
        .setCountry(address.getCountryName())
        .setCountryCode(address.getCountryCode())
        .setRegion(address.getLocality())
        .setCity(address.getSubLocality())
        .setTimezoneName(getTimezoneName())
        .setTimezoneOffset(getTimezoneOffset())
        .setAccuracy(location.getAccuracy())
        .createLocationInfo();
  }

  /**
   * @return
   */
  private String getTimezoneOffset() {
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
        Locale.getDefault());
    Date currentLocalTime = calendar.getTime();
    DateFormat date = new SimpleDateFormat("z", Locale.getDefault());

    return date.format(currentLocalTime);
  }

  private String getTimezoneName() {
    TimeZone current = TimeZone.getDefault();
    return current.getID();
  }


  public interface ParseCallback {
    void onLocationParsed(@Nullable LocationInfo parsedLocation);
  }

}
