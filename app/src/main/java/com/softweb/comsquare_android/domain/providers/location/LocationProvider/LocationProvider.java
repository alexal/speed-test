package com.softweb.comsquare_android.domain.providers.location.LocationProvider;

import android.support.annotation.NonNull;

import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;

public interface LocationProvider {

  void fetchLocation(LocationFetchListener listener);

  interface LocationFetchListener {
    void onLocationFetched(@NonNull LocationInfo location);

    void onError(@NonNull Exception e);
  }
}
