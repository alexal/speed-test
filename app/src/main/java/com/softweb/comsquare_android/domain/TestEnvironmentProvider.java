package com.softweb.comsquare_android.domain;

import com.softweb.comsquare_android.R;

import java.util.ArrayList;
import java.util.List;

public class TestEnvironmentProvider {

  // Singleton because no need for more than 1 instance, it's a data provider
  private static TestEnvironmentProvider INSTANCE;

  public static TestEnvironmentProvider getInstance(){
    if (INSTANCE == null)
      INSTANCE = new TestEnvironmentProvider();
    return INSTANCE;
  }

  private TestEnvironmentProvider(){}

  public ArrayList<TestEnvironmentType> getTestEnvironments(){

    ArrayList<TestEnvironmentType> result = new ArrayList<>();

    final TestEnvironmentType indoors = new TestEnvironmentType(TestEnvironmentType.STATIC_INDOOR, R.drawable.ic_home_black_24dp);
    final TestEnvironmentType outdoorsStatic = new TestEnvironmentType(TestEnvironmentType.STATIC_OUTDOOR, R.drawable.ic_nature_people_black_24dp);
    final TestEnvironmentType outdoorMoving = new TestEnvironmentType(TestEnvironmentType.MOVING_OUTDOOR, R.drawable.ic_outdoors_moving);

    result.add(indoors);result.add(outdoorsStatic);result.add(outdoorMoving);

    return result;
  }

}
