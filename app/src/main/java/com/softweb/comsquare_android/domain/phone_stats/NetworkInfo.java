package com.softweb.comsquare_android.domain.phone_stats;

public final class NetworkInfo {

  private final String ipAddress;
  private final boolean isIsp;
  private final String ispName;
  private final String serviceNetworkOperator;
  private final String servingMcc;
  private final String servingMnc;
  private final String homeNetworkOperator;
  private final String homeMcc;
  private final String homeMnc;
  private final String serviceNetworkOperator2; // For SIM 2
  private final String servingMcc2;
  private final String servingMnc2;
  private final String wifiSpeedMbps;

  private NetworkInfo(String ipAddress, boolean isIsp, String ispName, String serviceNetworkOperator, String servingMcc, String servingMnc, String homeNetworkOperator, String homeMcc, String homeMnc, String serviceNetworkOperator2, String servingMcc2, String servingMnc2, String wifiSpeedMbps) {
    this.ipAddress = ipAddress;
    this.isIsp = isIsp;
    this.ispName = ispName;
    this.serviceNetworkOperator = serviceNetworkOperator;
    this.servingMcc = servingMcc;
    this.servingMnc = servingMnc;
    this.homeNetworkOperator = homeNetworkOperator;
    this.homeMcc = homeMcc;
    this.homeMnc = homeMnc;
    this.serviceNetworkOperator2 = serviceNetworkOperator2;
    this.servingMcc2 = servingMcc2;
    this.servingMnc2 = servingMnc2;
    this.wifiSpeedMbps = wifiSpeedMbps;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public boolean isIsp() {
    return isIsp;
  }

  public String getIspName() {
    return ispName;
  }

  public String getServiceNetworkOperator() {
    return serviceNetworkOperator;
  }

  public String getServingMcc() {
    return servingMcc;
  }

  public String getServingMnc() {
    return servingMnc;
  }

  public String getHomeNetworkOperator() {
    return homeNetworkOperator;
  }

  public String getHomeMcc() {
    return homeMcc;
  }

  public String getHomeMnc() {
    return homeMnc;
  }

  public String getServiceNetworkOperator2() {
    return serviceNetworkOperator2;
  }

  public String getServingMcc2() {
    return servingMcc2;
  }

  public String getServingMnc2() {
    return servingMnc2;
  }

  public String getWifiSpeedMbps() {
    return wifiSpeedMbps;
  }

  @Override
  public String toString() {
    return "NetworkInfo{" +
        "ipAddress='" + ipAddress + '\'' +
        ", isIsp=" + isIsp +
        ", ispName='" + ispName + '\'' +
        ", serviceNetworkOperator='" + serviceNetworkOperator + '\'' +
        ", servingMcc='" + servingMcc + '\'' +
        ", servingMnc='" + servingMnc + '\'' +
        ", homeNetworkOperator='" + homeNetworkOperator + '\'' +
        ", homeMcc='" + homeMcc + '\'' +
        ", homeMnc='" + homeMnc + '\'' +
        ", serviceNetworkOperator2='" + serviceNetworkOperator2 + '\'' +
        ", servingMcc2='" + servingMcc2 + '\'' +
        ", servingMnc2='" + servingMnc2 + '\'' +
        ", wifiSpeedMbps='" + wifiSpeedMbps + '\'' +
        '}';
  }

  public static class Builder {
    private String ipAddress;
    private boolean isIsp;
    private String ispName;
    private String serviceNetworkOperator;
    private String servingMcc;
    private String servingMnc;
    private String homeNetworkOperator;
    private String homeMcc;
    private String homeMnc;
    private String serviceNetworkOperator2;
    private String servingMcc2;
    private String servingMnc2;
    private String wifiSpeedMbps;

    public Builder setIpAddress(String ipAddress) {
      this.ipAddress = ipAddress;
      return this;
    }

    public Builder setIsIsp(boolean isIsp) {
      this.isIsp = isIsp;
      return this;
    }

    public Builder setIspName(String ispName) {
      this.ispName = ispName;
      return this;
    }

    public Builder setServiceNetworkOperator(String serviceNetworkOperator) {
      this.serviceNetworkOperator = serviceNetworkOperator;
      return this;
    }

    public Builder setServingMcc(String servingMcc) {
      this.servingMcc = servingMcc;
      return this;
    }

    public Builder setServingMnc(String servingMnc) {
      this.servingMnc = servingMnc;
      return this;
    }

    public Builder setHomeNetworkOperator(String homeNetworkOperator) {
      this.homeNetworkOperator = homeNetworkOperator;
      return this;
    }

    public Builder setHomeMcc(String homeMcc) {
      this.homeMcc = homeMcc;
      return this;
    }

    public Builder setHomeMnc(String homeMnc) {
      this.homeMnc = homeMnc;
      return this;
    }

    public Builder setServiceNetworkOperator2(String serviceNetworkOperator2) {
      this.serviceNetworkOperator2 = serviceNetworkOperator2;
      return this;
    }

    public Builder setServingMcc2(String servingMcc2) {
      this.servingMcc2 = servingMcc2;
      return this;
    }

    public Builder setServingMnc2(String servingMnc2) {
      this.servingMnc2 = servingMnc2;
      return this;
    }

    public Builder setWifiSpeedMbps(String wifiSpeedMbps) {
      this.wifiSpeedMbps = wifiSpeedMbps;
      return this;
    }

    public NetworkInfo createNetworkInfo() {
      return new NetworkInfo(ipAddress, isIsp, ispName, serviceNetworkOperator, servingMcc, servingMnc, homeNetworkOperator, homeMcc, homeMnc, serviceNetworkOperator2, servingMcc2, servingMnc2, wifiSpeedMbps);
    }
  }

}
