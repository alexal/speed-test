package com.softweb.comsquare_android.domain.providers.network;

import android.annotation.SuppressLint;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.softweb.comsquare_android.domain.phone_stats.NetworkInfo;
import com.softweb.comsquare_android.utils.AppExecutors;

import java.util.List;

import timber.log.Timber;

public class NetworkInfoProvider {

  private final TelephonyManager telephonyManager;
  private final WifiManager wifiManager;
  private final IpProvider ipProvider;
  private final SubscriptionManager subscriptionManager;

  private NetworkInfoProvider(TelephonyManager pTelephonyManager,
                             WifiManager wifiManager,
                             IpProvider ipProvider,
                             SubscriptionManager subscriptionManager) {
    this.telephonyManager = pTelephonyManager;
    this.wifiManager = wifiManager;
    this.ipProvider = ipProvider;
    this.subscriptionManager = subscriptionManager;
  }

  /**
   * Fetches all the network info. Fetch of the info requires
   * ACCESS_WIFI_STATE permission, so make sure it's provided.
   * The execution happens by default on the network thread of app executors and the callback
   * is delivered on the main thread. Maybe later a custom configuration will be applied.
   */
  public NetworkInfo fetchNetworkInfo(){

      final NetworkInfo.Builder builder = new NetworkInfo.Builder();

      final String ipAddress = ipProvider.fetchProxyIPAddress(); // Synchronous call
      final String ispName = ipProvider.fetchISPName();
      final String serviceNetworkOperator = telephonyManager.getNetworkOperatorName();
      final String wifiSpeed = String.valueOf(wifiManager.getConnectionInfo().getLinkSpeed());

      builder.setIpAddress(ipAddress);
      builder.setIspName(ispName);
      builder.setServiceNetworkOperator(serviceNetworkOperator);
      builder.setWifiSpeedMbps(wifiSpeed);
      builder.setServingMcc(getServingMcc(telephonyManager));
      builder.setServingMnc(getServingMnc(telephonyManager));
      builder.setHomeNetworkOperator(getNetworkOperator(telephonyManager));
      builder.setHomeMcc(getHomeMcc(telephonyManager));
      builder.setHomeMnc(getHomeMnc(telephonyManager));
      builder.setServiceNetworkOperator2(getNetworkOperator2(subscriptionManager));
      builder.setServingMcc2(getServingMcc2(subscriptionManager));
      builder.setServingMnc2(getServiceMnc2(subscriptionManager));
      builder.setIsIsp(checkIfConnectionsIsToIsp());

      return builder.createNetworkInfo();
  }

  //TODO
  private boolean checkIfConnectionsIsToIsp(){
    return false;
  }

  @SuppressLint("MissingPermission")
  private String getServingMcc(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    int mcc = 0; // Unavailable

    if (firstAvailable instanceof CellInfoLte)
      mcc = ((CellInfoLte)firstAvailable).getCellIdentity().getMcc();

    if (firstAvailable instanceof CellInfoGsm)
      mcc = ((CellInfoGsm)firstAvailable).getCellIdentity().getMcc();

    if (firstAvailable instanceof CellInfoWcdma)
      mcc = ((CellInfoWcdma)firstAvailable).getCellIdentity().getMcc();

    return String.valueOf(mcc);
  }

  @SuppressLint("MissingPermission")
  private String getServingMnc(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    Timber.d("Cell info objects available = %s",allCellInfo.size());
    CellInfo firstAvailable = allCellInfo.get(0);

    int mnc = 0; // Unavailable

    if (firstAvailable instanceof CellInfoLte)
      mnc = ((CellInfoLte)firstAvailable).getCellIdentity().getMnc();

    if (firstAvailable instanceof CellInfoGsm)
      mnc = ((CellInfoGsm)firstAvailable).getCellIdentity().getMnc();

    if (firstAvailable instanceof CellInfoWcdma)
      mnc = ((CellInfoWcdma)firstAvailable).getCellIdentity().getMnc();

    return String.valueOf(mnc);
  }

  private String getNetworkOperator(TelephonyManager telephonyManager){
    return telephonyManager.getSimOperatorName();
  }

  private String getHomeMcc(TelephonyManager telephonyManager){

    String simOperator = telephonyManager.getSimOperator();
    return simOperator.substring(0,3);
  }

  private String getHomeMnc(TelephonyManager telephonyManager){

    String simOperator = telephonyManager.getSimOperator();
    return simOperator.substring(3, simOperator.length());
  }

  private String getNetworkOperator2(SubscriptionManager subscriptionManager){

    if (subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1) != null){
      return (String) subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getCarrierName();
    }
    return "Not available";
  }

  private String getServingMcc2(SubscriptionManager subscriptionManager){

    if (subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1) != null)
      return String.valueOf(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getMcc());

    return "Not available";
  }

  private String getServiceMnc2(SubscriptionManager subscriptionManager){

    if (subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1) != null)
      return String.valueOf(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getMnc());

    return "Not available";
  }

  // ----------------------------------------------------------------------------------------------

  /**
   * Builder class
   */
  public static class Builder {

    private TelephonyManager pTelephonyManager;
    private WifiManager wifiManager;
    private IpProvider ipProvider;
    private SubscriptionManager subscriptionManager;

    public Builder setpTelephonyManager(TelephonyManager pTelephonyManager) {
      this.pTelephonyManager = pTelephonyManager;
      return this;
    }

    public Builder setWifiManager(WifiManager wifiManager) {
      this.wifiManager = wifiManager;
      return this;
    }

    public Builder setSubscriptionManager(SubscriptionManager subscriptionManager){
      this.subscriptionManager = subscriptionManager;
      return this;
    }

    public Builder setIpProvider(IpProvider ipProvider) {
      this.ipProvider = ipProvider;
      return this;
    }

    public NetworkInfoProvider createNetworkInfoProvider() {
      return new NetworkInfoProvider(pTelephonyManager, wifiManager, ipProvider,subscriptionManager);
    }
  }

}
