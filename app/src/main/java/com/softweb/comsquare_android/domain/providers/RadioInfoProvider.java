package com.softweb.comsquare_android.domain.providers;

import android.annotation.SuppressLint;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import com.softweb.comsquare_android.domain.phone_stats.RadioInfo;
import com.softweb.comsquare_android.utils.Bus;

import java.util.List;

import timber.log.Timber;

// TODO Check if this should be a singleton
public class RadioInfoProvider {

  // --------------------- Constant keys ------------------------------->

  private static final int LAC_UKNOWN = 65535;
  private static final int LAC_LTE = -1; // LTE signals does not contain lac value
  private static final int TAC_NON_LTE = -1; // Non-lte signals do not contain tac value
  private static final int PSC_UKNOWN = 65535; //
  private static final int ASU_UKNOWN = 99;
  private static final int RSCP_UKNOWN = -200;
  private static final int ECIO_UKNOWN = 100;
  private static final int SINR_INVALID = 100;
  private static final String SIGNAL_UKNOWN = "signal_uknown";
  private static final int RSRP_UKNOWN = 0;
  private static final int RX_LEVEL_UKNOWN = 100;

  // ------------------------------------------------------------------->

  private final TelephonyManager mTelephonyManager;
  private final WifiManager mWifiManager;

  public RadioInfoProvider(TelephonyManager pTelephonyManager, WifiManager wifiManager) {
    mTelephonyManager = pTelephonyManager;
    mWifiManager = wifiManager;
  }

  @SuppressLint("MissingPermission")
  // TODO
  private double getCellId(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    double cellId;

    if (firstAvailable instanceof CellInfoLte){
      Timber.d(String.valueOf(((CellInfoLte) firstAvailable).getCellIdentity().getCi()));
    }

    if (firstAvailable instanceof CellInfoGsm){
      Timber.d(String.valueOf(((CellInfoGsm) firstAvailable).getCellIdentity().getCid()));
    }

    if (firstAvailable instanceof CellInfoWcdma){
      Timber.d(String.valueOf(((CellInfoWcdma) firstAvailable).getCellIdentity().getCid()));
    }

    return 0L;
  }

  @SuppressLint("MissingPermission")
  private int getLac(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      return LAC_UKNOWN;
    }

    if (firstAvailable instanceof CellInfoGsm){
      return ((CellInfoGsm) firstAvailable).getCellIdentity().getLac();
    }

    if (firstAvailable instanceof CellInfoWcdma){
      return ((CellInfoWcdma) firstAvailable).getCellIdentity().getLac();
    }

    return LAC_UKNOWN;
  }

  @SuppressLint("MissingPermission")
  private int getTac(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte)
      return ((CellInfoLte) firstAvailable).getCellIdentity().getTac();

    return TAC_NON_LTE;
  }

  @SuppressLint("MissingPermission")
  private int getPci(TelephonyManager telephonyManager){
    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte)
      return ((CellInfoLte) firstAvailable).getCellIdentity().getPci();

    return TAC_NON_LTE;
  }

  @SuppressLint("MissingPermission")
  private int getPsc(TelephonyManager telephonyManager){
    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoWcdma)
      return ((CellInfoWcdma) firstAvailable).getCellIdentity().getPsc();

    return PSC_UKNOWN;
  }

  @SuppressLint("MissingPermission")
  private int getBcch(TelephonyManager telephonyManager){
    return -1;
  }

  @SuppressLint("MissingPermission")
  private int getAsuLevel(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      return ((CellInfoLte) firstAvailable).getCellSignalStrength().getAsuLevel();
    }

    if (firstAvailable instanceof CellInfoGsm){
      return ((CellInfoGsm) firstAvailable).getCellSignalStrength().getAsuLevel();
    }

    if (firstAvailable instanceof CellInfoWcdma){
      return ((CellInfoWcdma) firstAvailable).getCellSignalStrength().getAsuLevel();
    }

    return ASU_UKNOWN;
  }

  private int getRscp(TelephonyManager telephonyManager){

    final int asuLevel = getAsuLevel(telephonyManager);

    if (asuLevel != ASU_UKNOWN)
      return asuLevel - 116;
    else
      return RSCP_UKNOWN;
  }

  @SuppressLint("MissingPermission")
  private int getLevel(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      return ((CellInfoLte) firstAvailable).getCellSignalStrength().getLevel();
    }

    if (firstAvailable instanceof CellInfoGsm){
      return ((CellInfoGsm) firstAvailable).getCellSignalStrength().getLevel();
    }

    if (firstAvailable instanceof CellInfoWcdma){
      return ((CellInfoWcdma) firstAvailable).getCellSignalStrength().getLevel();
    }

    return 0;
  }

  @SuppressLint("MissingPermission")
  private int getTimingAdvance(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){

      int ta =  ((CellInfoLte) firstAvailable).getCellSignalStrength().getTimingAdvance();
      return ta;
    }

    if (firstAvailable instanceof CellInfoGsm)
      return ((CellInfoGsm) firstAvailable).getCellSignalStrength().getTimingAdvance();

    return 0;
  }

  @SuppressLint("MissingPermission")
  private int getEcio(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoWcdma){

      final CellSignalStrengthWcdma signalStrength = ((CellInfoWcdma) firstAvailable).getCellSignalStrength();
      final int asuLevel = signalStrength.getAsuLevel();
      final int rscp = asuLevel - 116;
      final int rssi = signalStrength.getDbm();

      return rscp - rssi;
    }

    return ECIO_UKNOWN;
  }

  /**
   * Get the integer value between the first and second strings in the query string.
   * @param query the whole string which contains the information
   * @param first the string whose value you want to parse for example: rsrp
   * @param second the string key of the next value in the query string, for example: rsrq value is reported after rsrp.
   * @return
   */
  private int getIntValue(String query, String first, String second){

    final int firstIndex =  query.indexOf(first);
    final int secondIndex = query.indexOf(second);

    // Both first and second values have an '=' after the numeric value so remove it
    final int beginIndex = firstIndex + first.length() + 1;
    final int endIndex = secondIndex - 1;

    final String result = query.substring(beginIndex, endIndex);

    return Integer.valueOf(result);
  }

  private int parseSinr(String signalStrength){
    return getIntValue(signalStrength, "rssnr","cqi");
  }

  @SuppressLint("MissingPermission")
  private int getSinr(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){

      final CellSignalStrengthLte signalStrength = ((CellInfoLte) firstAvailable).getCellSignalStrength();

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        return signalStrength.getRssnr();
      else
        return parseSinr(signalStrength.toString());
    }
    return SINR_INVALID;
  }

  @SuppressLint("MissingPermission")
  private String getSignalString(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte)
      return ((CellInfoLte) firstAvailable).getCellSignalStrength().toString();

    if (firstAvailable instanceof CellInfoGsm)
      return ((CellInfoGsm) firstAvailable).getCellSignalStrength().toString();

    if (firstAvailable instanceof CellInfoWcdma)
      return ((CellInfoWcdma) firstAvailable).getCellSignalStrength().toString();

    if (firstAvailable instanceof CellInfoCdma)
      return ((CellInfoCdma)firstAvailable).getCellSignalStrength().toString();

    return SIGNAL_UKNOWN;
  }

  private String getWifiFrequencyBand(WifiManager wifiManager){
    final int frequency = wifiManager.getConnectionInfo().getFrequency();
    if (frequency > 2495)
      return "5 GHz";
    else
      return "2.4 GHz";
  }

  private String getFrequency(WifiManager wifiManager){
    return String.format("%s MHz", wifiManager.getConnectionInfo().getFrequency());
  }

  private int parseRsrp(String signalStrength){
    return getIntValue(signalStrength, "rsrp", "rsrq");
  }

  @SuppressLint("MissingPermission")
  private int getRsrp(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        return ((CellInfoLte) firstAvailable).getCellSignalStrength().getRsrp();
      else
        return parseRsrp(((CellInfoLte) firstAvailable).getCellSignalStrength().toString());
    }

    return RSRP_UKNOWN;
  }

  private int parseRsrq(String signalStrength){
    return getIntValue(signalStrength, "rsrq", "rssnr");
  }

  @SuppressLint("MissingPermission")
  private int getRsrq(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        return ((CellInfoLte) firstAvailable).getCellSignalStrength().getRsrq();
      }
      else
        return parseRsrq(((CellInfoLte) firstAvailable).getCellSignalStrength().toString());
    }

    return RSRP_UKNOWN;
  }

  private int parseCqi(String signalStrength){
    return getIntValue(signalStrength, "cqi", "ta");
  }

  @SuppressLint("MissingPermission")
  private int getCqi(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte){
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        return ((CellInfoLte) firstAvailable).getCellSignalStrength().getCqi();
      else
        return parseCqi(((CellInfoLte) firstAvailable).getCellSignalStrength().toString());
    }

    return RSRP_UKNOWN;
  }

  @SuppressLint("MissingPermission")
  private int getRxLevel(TelephonyManager telephonyManager){

    List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
    CellInfo firstAvailable = allCellInfo.get(0);

    if (firstAvailable instanceof CellInfoLte)
        return ((CellInfoLte) firstAvailable).getCellSignalStrength().getAsuLevel() - 113;

    return RX_LEVEL_UKNOWN;
  }

  public RadioInfo fetchRadioInfo() {

    final RadioInfo.Builder builder = new RadioInfo.Builder();

    final String bssID = mWifiManager.getConnectionInfo().getBSSID();
    final String ssID = mWifiManager.getConnectionInfo().getSSID();
    final int rssi = mWifiManager.getConnectionInfo().getRssi();
    final int lac = getLac(mTelephonyManager);
    final int tac = getTac(mTelephonyManager);
    final int pci = getPci(mTelephonyManager);
    final int psc = getPsc(mTelephonyManager);
    final int bcch = getBcch(mTelephonyManager);
    final int asuLevel = getAsuLevel(mTelephonyManager);
    final int rscp = getRscp(mTelephonyManager);
    final int level = getLevel(mTelephonyManager);
    final int timingAdvance = getTimingAdvance(mTelephonyManager);
    final int ecio = getEcio(mTelephonyManager);
    final int sinr = getSinr(mTelephonyManager);
    final String signalString = getSignalString(mTelephonyManager);
    final String wifiFrequencyBand = getWifiFrequencyBand(mWifiManager);
    final String frequency = getFrequency(mWifiManager);
    final int rsrp = getRsrp(mTelephonyManager);
    final int rsrq = getRsrq(mTelephonyManager);
    final int cqi = getCqi(mTelephonyManager);
    final int rxLevel = getRxLevel(mTelephonyManager);

    builder.setWifiBssid(bssID)
        .setWifiSsid(ssID)
        .setRssi(rssi)
        .setLac(lac)
        .setTac(tac)
        .setPci(pci)
        .setPsc(psc)
        .setBcch(bcch)
        .setAsu(asuLevel)
        .setRscp(rscp)
        .setLevel(level)
        .setSinr(sinr)
        .setEcio(ecio)
        .setSignalString(signalString)
        .setWifiFrequencyBandMhz(wifiFrequencyBand)
        .setWifiFrequencyMhz(frequency)
        .setRsrp(rsrp)
        .setRsrq(rsrq)
        .setCqi(cqi)
        .setRxLevel(rxLevel)
        .setTimingAdvance(timingAdvance);

    return builder.createRadioInfo();
  }

  // --------------------------------------------------------------------------------------------- >

  public void fetchCurrentCellId(RadioInfo radioInfo, boolean startingCellValue){

    if (startingCellValue)
      radioInfo.setStartCellId(String.valueOf(getCellId(mTelephonyManager)));
    else
      radioInfo.setEndCellId(String.valueOf(getCellId(mTelephonyManager)));
  }

  public void fetchCurrentRnc(RadioInfo radioInfo){

  }

  private static class MyPhoneStateListener extends PhoneStateListener {

    private int signalStrengthValue;

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
      super.onSignalStrengthsChanged(signalStrength);

      if (signalStrength.isGsm()) {
        if (signalStrength.getGsmSignalStrength() != 99)
          signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
        else
          signalStrengthValue = signalStrength.getGsmSignalStrength();
      } else {
        signalStrengthValue = signalStrength.getCdmaDbm();
      }

      // Also post an event to the bus for any intersted provider
      Bus.post(new SignalStrengthChangeEvent(signalStrengthValue));
    }
  }

  /**
   *  Event class posted through BUS utility to inform anyone about signal strength value change.
   */
  public static class SignalStrengthChangeEvent extends Bus.EventWithArgs<Integer> {

    public SignalStrengthChangeEvent(Integer argument) {
      super(argument);
    }
  }

  private String parsePhoneType(int phoneType) {
    switch (phoneType) {
      case TelephonyManager.PHONE_TYPE_NONE:
        return "Not available";
      case TelephonyManager.PHONE_TYPE_CDMA:
        return "CDMA ";
      case TelephonyManager.PHONE_TYPE_GSM:
        return "GSM";
      case TelephonyManager.PHONE_TYPE_SIP:
        return "SIP";
    }
    return "Unknown radio type";
  }

}
