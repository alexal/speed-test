package com.softweb.comsquare_android.domain;

import com.softweb.comsquare_android.domain.phone_stats.DeviceInfo;
import com.softweb.comsquare_android.domain.phone_stats.ExportInfo;
import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;
import com.softweb.comsquare_android.domain.phone_stats.NetworkInfo;
import com.softweb.comsquare_android.domain.phone_stats.RadioInfo;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

/**
 * Class to export all the required variables to JSON file.
 */
public final class ExportJSONHelper {

  private final Moshi moshi;
  private final ExportInfo exportInfo;

  public ExportJSONHelper(ExportInfo exportInfo){

    this.exportInfo = exportInfo;
    moshi = new Moshi.Builder().build();
  }

  public String exportLocationInfo(){

    JsonAdapter<LocationInfo> locationAdapter = moshi.adapter(LocationInfo.class);

    return locationAdapter.toJson(exportInfo.getLocationInfo());
  }

  public String exportDeviceInfo(){

    JsonAdapter<DeviceInfo> deviceAdapter = moshi.adapter(DeviceInfo.class);

    return deviceAdapter.toJson(exportInfo.getDeviceInfo());
  }

  public String exportNetworkInfo(){

    JsonAdapter<NetworkInfo> adapter = moshi.adapter(NetworkInfo.class);

    return adapter.toJson(exportInfo.getNetworkInfo());
  }

  public String exportRadioInfo(){

    JsonAdapter<RadioInfo> adapter = moshi.adapter(RadioInfo.class);

    return adapter.toJson(exportInfo.getRadioInfo());
  }

  public String exportAllInfo(){

    JsonAdapter<ExportInfo> adapter = moshi.adapter(ExportInfo.class);

    return adapter.toJson(exportInfo);
  }

}
