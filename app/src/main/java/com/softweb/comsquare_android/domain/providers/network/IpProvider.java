package com.softweb.comsquare_android.domain.providers.network;

public interface IpProvider {
  String fetchProxyIPAddress();
  String fetchISPName();
}
