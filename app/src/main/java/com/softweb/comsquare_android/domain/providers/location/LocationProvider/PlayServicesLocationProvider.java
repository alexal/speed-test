package com.softweb.comsquare_android.domain.providers.location.LocationProvider;

import android.support.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.softweb.comsquare_android.domain.phone_stats.LocationInfo;

public class PlayServicesLocationProvider implements LocationProvider{

  private final FusedLocationProviderClient fusedLocationProviderClient;
  private final LocationParser mLocationParser;
  private LocationInfo cachedLocationInfo;

  public PlayServicesLocationProvider(FusedLocationProviderClient pLocationClient, LocationParser locationParser) {
    fusedLocationProviderClient = pLocationClient;
    mLocationParser = locationParser;
  }

  /**
   * Get the last known location of the device via the callback.
   * The results are cached, meaning that the next call on this same function after a
   * successful fetch, will return the same cached value.
   * <p>
   * The location returned can be null in the following cases:
   * - LocationInfo is turned off in the device
   * - LocationInfo was never recorded
   * <p>
   * It is advisable to check the nullability of the returned object
   *
   * @param listener Callback triggered when the location is fetched
   * @throws SecurityException if the user has not granted LOCATION permission
   */
  @Override
  public void fetchLocation(@NonNull LocationProvider.LocationFetchListener listener) throws SecurityException {

    if (cachedLocationInfo != null){
      listener.onLocationFetched(cachedLocationInfo);
      return;
    }

    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(
        location -> mLocationParser.parse(location, parsedLocation -> {
          if (parsedLocation != null) {
            listener.onLocationFetched(parsedLocation);
            cachedLocationInfo = parsedLocation;
          } else
            listener.onError(new Exception("Geocoder fail. Please reboot your device"));
        }))
        .addOnFailureListener(listener::onError);
  }


}
