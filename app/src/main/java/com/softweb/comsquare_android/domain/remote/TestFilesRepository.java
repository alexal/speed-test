package com.softweb.comsquare_android.domain.remote;

import android.support.annotation.StringDef;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressListener;
import okhttp3.Call;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class TestFilesRepository {

  // ----------------------- Available file sizes in the server -------------------------->

  public static final String MB_1 = "1M";
  public static final String MB_3 = "3M";
  public static final String MB_5 = "5M";
  public static final String MB_10 = "10M";
  public static final String MB_12 = "12M";
  public static final String MB_50 = "50M";
  public static final String MB_100 = "100M";
  public static final String MB_180 = "180M";
  public static final String MB_200 = "200M";
  public static final String MB_1500 = "1500M";

  @StringDef({MB_1,MB_3,MB_5,MB_10,MB_12,MB_50,MB_100,MB_180,MB_200,MB_1500})
  @Retention(RetentionPolicy.SOURCE)
  public @interface FileID{}

  // --------------------------------------------------------------------------------------

  private static final String SERVER_URL = "http://77.95.228.23/";
  private static final String UPLOAD_ROUTE = "put_no_save.php";

  private static TestFilesRepository INSTANCE;
  private boolean keepFetching;

  public static TestFilesRepository getINSTANCE(){
    if (INSTANCE == null)
      INSTANCE = new TestFilesRepository();
    return INSTANCE;
  }

  /**
   * Configuration class of the repository.
   */
  public static class Config {
    File cacheFile;

  }

  public interface DownloadProgressListener {
    void onBytesFetched(long downloaded, long total, byte[] bytes);
  }

  public interface UploadProgressListener {
    void onBytesUploaded(long uploaded, long total, float percent, float speed);
  }

  public TestFilesRepository(){}

  public void cancelFetch(){
    keepFetching = false;
  }

  public void fetchTestFile(@FileID final String testFileID, final DownloadProgressListener downloadProgressListener) {

    final String downloadURL = SERVER_URL + testFileID;
    keepFetching = true;

    final Request request = new Request.Builder().url(downloadURL).build();
    final OkHttpClient client = new OkHttpClient();
    final Call call = client.newCall(request);

    try {

      Response response = call.execute();

      if (response.code() == 200 ){

        InputStream inputStream = null;

        try {

          inputStream = response.body().byteStream();

          byte[] buff = new byte[1024 * 4];
          long downloaded = 0;
          long target = response.body().contentLength();

          downloadProgressListener.onBytesFetched(0, target, buff);

          while (keepFetching) {
            int readed = inputStream.read(buff);
            if (readed == -1) {
              break;
            }
            //write buff
            downloaded += readed;
            downloadProgressListener.onBytesFetched(downloaded, target, buff);
          }
        } catch (IOException ignore) {
          ignore.printStackTrace();
        } finally {
          if (inputStream != null) {
            inputStream.close();
          }
        }
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void uploadTestFile(File file, UploadProgressListener uploadProgressListener){

    final OkHttpClient okHttpClient = new OkHttpClient();
    final String uploadUrl = SERVER_URL + UPLOAD_ROUTE;
    final Request.Builder builder = new Request.Builder().url(uploadUrl);

    final MultipartBody.Builder multipartBodyBuilder = new MultipartBody.Builder();
    multipartBodyBuilder.addFormDataPart("testFile", file.getName(), RequestBody.create(null, file));
    MultipartBody body = multipartBodyBuilder.build();

    RequestBody requestBody = ProgressHelper.withProgress(body, new ProgressListener() {
      @Override
      public void onProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
        uploadProgressListener.onBytesUploaded(numBytes, totalBytes, percent, speed);
      }
    });

    builder.post(requestBody);

    okHttpClient.newCall(builder.build());
  }
}

