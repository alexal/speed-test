package com.softweb.comsquare_android.domain.providers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.softweb.comsquare_android.domain.phone_stats.DeviceInfo;
import com.softweb.comsquare_android.utils.App;

import io.fabric.sdk.android.services.common.CommonUtils;

public class DeviceInfoProvider {

  private final TelephonyManager telephonyManager;
  private final PackageManager packageManager;
  private final Context context;

  public DeviceInfoProvider(final TelephonyManager telephonyManager,
                            PackageManager packageManager,
                            Context context) {
    this.telephonyManager = telephonyManager;
    this.packageManager = packageManager;
    this.context = context;
  }

  /**
   * Computes all the necessary device info. The result will be delivered via the callback.
   * It is advisable to run this in a background thread due to some time-consuming tasks.
   */
  public DeviceInfo fetchDeviceInfo() {

    final DeviceInfo.Builder builder = new DeviceInfo.Builder();

    builder.setOsVersion("Android " + Build.VERSION.SDK_INT)
        .setBrand(Build.BRAND)
        .setDevice(Build.DEVICE)
        .setHardwareChipset(Build.HARDWARE)
        .setBuildId(Build.ID)
        .setFirmwareVersion(Build.getRadioVersion())
        .setManufacturer(Build.MANUFACTURER)
        .setModel(Build.MODEL)
        .setAppVersion(getVersionName(packageManager, context))
        .setImeiIsValid(imeiIsValid(telephonyManager))
        .setDeviceIsRooted(isRooted(context))
        .setApplicationID(App.getCustomerID(context));

    return builder.createDeviceInfo();
  }

  private String getVersionName(PackageManager packageManager, Context context) {
    String versionName;
    try {
      PackageInfo pInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
      versionName = pInfo.versionName;
    } catch (PackageManager.NameNotFoundException e) {
      versionName = "Unknown version name";
    }
    return versionName;
  }

  private boolean isRooted(Context context) {
    return CommonUtils.isRooted(context); // Using Fabric util method to determine it
  }

  private boolean imeiIsValid(TelephonyManager telephonyManager) {

    // First try imei
    final String imei = getImei();
    final boolean isImeiValid = isValidImei(imei);
    if (isImeiValid)
      return true;
    else {
      // Try meid
      final String meid = getMeid();
      final boolean isMeidValid = isValidImei(meid);
      return isMeidValid;
    }
  }

  @SuppressLint("MissingPermission")
  private String getImei() {
    String imei;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
      imei = telephonyManager.getImei();
    else
      imei = telephonyManager.getDeviceId();
    return imei;
  }

  @SuppressLint("MissingPermission")
  private String getMeid() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      return telephonyManager.getMeid();
    }
    return "";
  }

  private boolean isValidImei(String s) {
    long n = Long.parseLong(s);
    int l = s.length();

    if (l != 15) // If length is not 15 then IMEI is Invalid
      return false;
    else {
      int d = 0, sum = 0;
      for (int i = 15; i >= 1; i--) {
        d = (int) (n % 10);
        if (i % 2 == 0) {
          d = 2 * d; // Doubling every alternate digit
        }

        sum = sum + sumDig(d); // Finding sum of the digits

        n = n / 10;
      }

      return sum % 10 == 0 && sum != 0;
    }
  }

  /**
   * Function for finding and returning sum of digits of a number
   */
  private int sumDig(int n) {
    int a = 0;
    while (n > 0) {
      a = a + n % 10;
      n = n / 10;
    }
    return a;
  }
}
