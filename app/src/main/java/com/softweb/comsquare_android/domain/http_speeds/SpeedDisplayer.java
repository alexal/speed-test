package com.softweb.comsquare_android.domain.http_speeds;

/**
 *
 */
public interface SpeedDisplayer {
  String getDisplayedSpeed(final long startMillis, final long currentMillis, final long currentSize);
}
