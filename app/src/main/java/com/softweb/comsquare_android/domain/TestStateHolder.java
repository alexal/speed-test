package com.softweb.comsquare_android.domain;

public final class TestStateHolder {

  private TestEnvironmentType testEnvironmentType;

  public TestStateHolder(){}

  public void setTestEnvironmentType(TestEnvironmentType testEnvironmentType) {
    this.testEnvironmentType = testEnvironmentType;
  }
}
