package com.softweb.comsquare_android.domain.phone_stats;

public final class ExportInfo {

  private final DeviceInfo deviceInfo;
  private final RadioInfo radioInfo;
  private final NetworkInfo networkInfo;
  private final LocationInfo locationInfo;

  public ExportInfo(DeviceInfo deviceInfo, RadioInfo radioInfo, NetworkInfo networkInfo, LocationInfo locationInfo) {
    this.deviceInfo = deviceInfo;
    this.radioInfo = radioInfo;
    this.networkInfo = networkInfo;
    this.locationInfo = locationInfo;
  }

  public DeviceInfo getDeviceInfo() {
    return deviceInfo;
  }

  public LocationInfo getLocationInfo() {
    return locationInfo;
  }

  public NetworkInfo getNetworkInfo() {
    return networkInfo;
  }

  public RadioInfo getRadioInfo() {
    return radioInfo;
  }
}
