package com.softweb.comsquare_android;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.softweb.comsquare_android.utils.AppExecutors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class CommSquare extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    // TODO Enable on production
    // Fabric.with(this,new Crashlytics());

    Timber.plant(new Timber.DebugTree());

    // Init app threads with lazy loading
    AppExecutors.getInstance();
  }

  private static class CrashlyticsTree extends Timber.Tree {

    @Override
    protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {

    }
  }
}
